* see maintenance/f2022.R
* ist es moeglich, die Gruppierung wie die auswahl zu gestalten? dazu:
  - entscheiden, ob die werte distincte Klassen oder breaks einer numerischen
    Gruppierung sind.
  - Alle "Randsummen" ueber die Gruppierung bilden.
* in fvbnX.R, we should be able to pass an arbitrary vector to argument D.klass.
* add logical subsets to predict
* Fix FIXMEs in R/BWI3_HR.R and R/fvbn2e.R: check with Gerald. There will be
  loads of them in other functions!
* Write data input checks to make sure BWI4 input data will conform to the tables in BWI3.
* reconsider the package_name-option

= remember
* xtable's column ordering in R changes with different versions of R!

= standards 
* Improve test coverage
* clean, lint, spell!

= wtf
* pull documentation from deleted files 
  R/data\_documentation.R and
  R/global\_variables.R
* add exclusions coverage
* use a set_options(use_global_variables) and move graphics\_[width|height] to
  getter functions reading that option. -- Haven't I done this?!
 
