% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/VB.A.bagrupp.akl.dkl.stratum.fun.2.R
\name{VB.A.bagrupp.akl.dkl.stratum.fun.2}
\alias{VB.A.bagrupp.akl.dkl.stratum.fun.2}
\title{Aggregiert den ausgeschiedenen Vorrat}
\usage{
VB.A.bagrupp.akl.dkl.stratum.fun.2(
  BA.grupp,
  A.klass,
  D.klass,
  auswahl,
  N.art,
  A,
  baeume.3 = get_data("baeume.3"),
  trakte.3 = get_data("trakte.3"),
  ecken.3 = get_data("ecken.3"),
  trakte.2 = get_data("trakte.2"),
  ecken.2 = get_data("ecken.2"),
  baeume.23 = get_data("baeume.23")
)
}
\arguments{
\item{BA.grupp}{Liste mit Baumarten-Zusammenfassungen zu Baumgruppen mit
Bezeichner der Baumarten-Gruppen ("lab") z.B. list(bagr.lab = c("FiTa",
"DglKiLae", "Bu", "Ei", "BLb", "WLb"), ba.grupp =list(c(10:19,30:39,90:99),
c(20:29,40,50,51), c(100), c(110,111), c(112:199),c(200:299))).}

\item{A.klass}{Liste mit den Klassifizierungsparametern fuers Alter: z.B.
list(A.ob=160, A.b=20).}

\item{D.klass}{Liste mit den Klassifizierungsparametern fuer Durchmesser z.B.
list(D.unt=0, D.ob=70, D.b=10, Ndh=T), Ndh (Nicht-Derbholz) = T bedeutet,
dass zusaetzlich Nicht-Dh (unter 7 cm) ausgewiesen wird, sonst gilt
\code{D.unt} als unterste Schwelle.}

\item{auswahl}{Liste, welche die Eckenmerkmale mit den Werten
enthaelt, anhand derer die Auswahl fuer das Stratum erfolgt. Bsp.:
list(Wa=c(3,5), Begehbar=1).}

\item{N.art}{Differenzierung nach Nutzungsart. TRUE: Trennung nach "geerntet"
= Pk 2,3,9 und "ungenutzt" = Pk 4, 5; FALSE: keine Trennung!}

\item{A}{Gesamtflaeche des Inventurgebiets in ha zum jeweiligen
Inventurzeitpunkt (sollte eigentlich konstant sein).}

\item{baeume.3}{Tree information.}

\item{trakte.3}{Trakt information.}

\item{ecken.3}{Ecken information.}

\item{trakte.2}{Trakt information.}

\item{ecken.2}{Ecken information.}

\item{baeume.23}{Tabelle mit Baumdaten aus BWI 2.}
}
\value{
Liste mit folgenden Komponenten: \strong{Log} (Liste mit Datum und
 genutzter Baumversion), \strong{Stratum} (\code{auswahl}), \strong{nTE}
 (Anzahl Ecken im Stratum), \strong{HBF} (Holzbodenflaeche in ha),
 \strong{se.HBF} (Standardfehler der HBF), \strong{Attribute} (Vektor mir
 berechneten Attributen), \strong{Groessen} (Vektor mit berechneten Groessen
 fuer Attribute (Wert, Standardfehler)), \strong{Nutzungsart} (2 Kategorien:
 geernet oder ungenutzt, wenn diese nicht definiert sind, wird "insgesamt"
 ausgegeben), \strong{BAGR} (Labels fuer Baumartengruppen aus
 \code{ba.grupp}), \strong{AKL} (Labels der Altersklassen), \strong{DKL}
 (Labels der Durchmesserklassen), \strong{T.VBN.A.NArt.Bagr.Akl.Dkl} (Array
 mit berechneten Groessen (Wert und Standardfehler) fuer das ausgeschiedene
 Kollektiv), \strong{BAF.bagr.akl.dkl} (Array mit Werten und Standardfehlern
 zu Baumartenflaechen nach Baumartengruppen, Alterklassen und
 Durchmesserklassen), \strong{mPL.NArt.Bagr.Akl.Dkl} (mittlere kalkulierte
 Periodenlaenge mit Standardfehler), \strong{mPL.Stratum} (mittlere
 Periodenlaenge als gewogenes Mittel), \strong{se.mPL.Stratum}
 (Standardfehler der mittleren Periodenlaenge (mPl.Stratum)),
 \strong{nT.NArt.Bagr.Akl.Dkl} (Anzahl Trakte je Nutzungsart,
 Baumartengruppen, Altersklassen und Durchmesserklassen).
}
\description{
Funktion wertet nach freien Baumarten-Gruppen und Alters- und Durchmesser-
Klassen im Stratum den ausgeschiedenen Vorrat aus (Derbholz mR, Erntevolumen
oR, oberird. Biomasse oiB). Im Unterschied zu
\code{VB.A.bagrupp.akl.dkl.stratum.fun.3} wird hier als 4. Groesse die
ausgeschiedene Stammzahl berechnet (in Version .3 das Erntevolumen im
Hauptbestand).
}
\section{Erstellungsdatum}{
 14.03.2014
}

\section{Achtung}{
 Voraussetzung ist, dass die Tabellen \code{baeume.23}
 (Baeume_B-Tabelle der Vorinventur (fuer BWI 3 die Tabelle
 "BWI_23_Baeume_B"), folgende Attribute muessen mind. enthalten sein: TNr,
 ENr, BA, Pk, Alt2, BHD2, VolV2, oiB2, NHa1, StFl1. Man kann auch die
 selektierten Attribute mit Namen ohne Kennziffer uebergeben, wenn bereits
 eine eindeutige Auswahl der Attribute in <baeume> uebergeben wurde.),
 \code{baeume.3}, (Baeume der Folgeinventur) \code{ecken.2}, \code{ecken.3},
 \code{trakte.3} sowie \code{bacode} eingelesen sind! \cr
 Das Baumattribut <Pk> muss enthalten sein!
}

\section{Hinweis}{
 Version mit freier Baumartengruppierung sowie jaehrlicher
 flaechenbezogenen Nutzung analog flaechenbezogenem jaehrlichen Zuwachs sowie
 Option einer Differenzierung nach Nutzungsart. \cr
 Ratio-Schaetzer-Varianz ueber \code{r.variance.fun} (Matrizen-Notation)
 berechnet! \cr
 Version fuer Periode BWI 2 zu 3! \cr
 Auswertung erfolgt auf dem gemeinsamen Netz im Unterschied zu
 der historischen Version VB.A.BAGR.akl.dkl.stratum.fun, welche das Nutzungsgeschehen
 auf der bei der BWI 2 erfassten Flaeche abdeckte! \cr
 Fuer die Berechnung der flaechenbezogenen Nutzungen muss die mittlere
 Baumartenflaeche der Periode berechnet werden, d.h. es werden auch die
 Standflaechen der Folgeaufnahme benoetigt! \cr
 Dies wird von der Funktion \code{\link{mbaf.bagr.alt.bhd.pm.fun}}
 uebernommen! \cr
 Um Konflikte mit unterschiedlicher Gross- und Kleinschreibung bei den
 Attributnamen zu vermeiden, werden innerhalb dieser Funktion alle
 Attributnamen auf Kleinschreibung umgestellt.
}

\section{TODO}{
 Verallgemeinerung bzw. Variante fuer BWI 1 zu 2 !!!!
}

\examples{
bagrupp <- list(
  bagr.lab = c("FiTa", "DglKiLae", "Bu", "Ei", "BLb", "WLb"),
  ba.grupp = list(
    c(10:19, 30:39, 90:99), c(20:29, 40, 50, 51),
    c(100), c(110, 111), c(112:199), c(200:299)
  )
)
aklass <- list(A.ob = 160, A.b = 20)
dklass <- list(D.unt = 0, D.ob = 70, D.b = 10, Ndh = TRUE)
auswahl <- list(Wa = c(3, 5), Begehbar = 1)
A <- bundeswaldinventur::get_design("A", 3)
r <- VB.A.bagrupp.akl.dkl.stratum.fun.2(
  BA.grupp = bagrupp,
  A.klass = aklass,
  D.klass = dklass,
  auswahl = auswahl,
  N.art = TRUE,
  A = A,
  baeume.3 = get_data("baeume.3"),
  trakte.3 = get_data("trakte.3"),
  ecken.3 = get_data("ecken.3"),
  trakte.2 = get_data("trakte.2"),
  ecken.2 = get_data("ecken.2"),
  baeume.23 = get_data("baeume.23")
)
}
\author{
Gerald Kaendler \email{gerald.kaendler@forst.bwl.de}
}
