% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/statistics.R
\name{modal_value}
\alias{modal_value}
\title{compute the mode/modal value from a set of discrete values}
\usage{
modal_value(x, multiple = FALSE)
}
\arguments{
\item{x}{a vector of values.}

\item{multiple}{return multiple modal values (or just the first one)?}
}
\value{
the modal value(s) of x.
}
\description{
base::mean() and stats::median() are part of R, mode() returns the (S-)
storage mode or type of its argument. A modal function is simply missing.
The fact is well known, there different solutions, this one is based on
https://stat.ethz.ch/pipermail/r-help/2001-August/014677.html and
http://stackoverflow.com/questions/2547402/standard-library-function-in-r-for-finding-the-mode.
}
\author{
Dominik Cullmann <dominik.cullmann@forst.bwl.de>
}
