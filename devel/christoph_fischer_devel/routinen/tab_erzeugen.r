# Erstellt f�r Hochrechnung erforderliche Eingangstabellen aus TI-Datenmodell
# Tabellen werden als workspace abgelegt
# Stand BWI3, muss f�r jede Inventur neu angepasst werden
# bisher nur ODBC- Schnittstelle

#-------------------------------------------------------------------------------
# Pfad und DB-Anbindung
library(RODBC)
pfad="db/bwi_1014.mdb"
channel=odbcConnectAccess(pfad)

# Ausgabe f�r workspace
pfadaus="tab/tabellen.RData"

#-------------------------------------------------------------------------------
# 1. Zustand BWI3: Tabelle mit allen Trakten, Fl�chengr��en, Anzahl Ecken je Trakt

ges1 = sqlQuery(channel,paste("SELECT dat_b0_ecke.Tnr, dat_b0_ecke.Enr, dat_b0_ecke.VBl, dat_b0_ecke.Bl,
                                  xyk_x_vbl.Ah3 AS Flaeche
                               FROM dat_b0_ecke INNER JOIN xyk_x_vbl ON dat_b0_ecke.VBl = xyk_x_vbl.ICode
                               WHERE dat_b0_ecke.InvE3=1;"))


# Eindeutige Ecknummer
#ges1$id=paste(ges1$Tnr, ges1$Enr, sep="")

# Anzahl Traktecken je Verdichtungsgebiet
ges1$n_ecken_vbl=ave(ges1$Enr, ges1$VBl, FUN=length)

# Ecken je Trakt
ges1$n_ecken_trakt=ave(ges1$Enr, ges1$VBl, ges1$Tnr, FUN=length)

# nur Trakte behalten
ges1=unique(ges1[,c("Tnr", "VBl", "Bl", "Flaeche", "n_ecken_vbl", "n_ecken_trakt")])
b0_ecke_z=ges1

#-------------------------------------------------------------------------------
# 2. Ver�nderung BWI2-BWI3: Tabelle mit allen Trakten, Fl�chengr��en, Anzahl Ecken je Trakt

ges1 = sqlQuery(channel,paste("SELECT dat_b0_ecke.Tnr, dat_b0_ecke.Enr, dat_b0_ecke.VBl, dat_b0_ecke.Bl,
                                  xyk_x_vbl.Ah3 AS Flaeche
                               FROM dat_b0_ecke INNER JOIN xyk_x_vbl ON dat_b0_ecke.VBl = xyk_x_vbl.ICode
                               WHERE dat_b0_ecke.InvE3=1 AND dat_b0_ecke.InvE2=1;"))

# Eindeutige Ecknummer
#ges1$id=paste(ges1$Tnr, ges1$Enr, sep="")

# Anzahl Traktecken je Verdichtungsgebiet
ges1$n_ecken_vbl=ave(ges1$Enr, ges1$VBl, FUN=length)

# Ecken je Trakt
ges1$n_ecken_trakt=ave(ges1$Enr, ges1$VBl, ges1$Tnr, FUN=length)

# nur Trakte behalten
ges1=unique(ges1[,c("Tnr", "VBl", "Bl", "Flaeche", "n_ecken_vbl", "n_ecken_trakt")])
b0_ecke_v=ges1

#-------------------------------------------------------------------------------
# 3. Vereinigungsmenge Waldecken mit Kennung f�r interne Korrektur bei Ver�nderungsrechnung (nicht gleicher Erhebungsort, r�ckwirkend Wald, etc)

woa = sqlQuery(channel,paste("SELECT dat_b0_ecke.Tnr, dat_b0_ecke.Enr, dat_b0_ecke.Use23,
                                dat_b23_ecke_w.WaldEntw
                              FROM dat_b0_ecke INNER JOIN dat_b23_ecke_w ON dat_b0_ecke.Tnr = dat_b23_ecke_w.Tnr AND dat_b0_ecke.Enr = dat_b23_ecke_w.Enr
                              WHERE dat_b23_ecke_w.WaldEntw=3 AND
                                dat_b0_ecke.InvE2=1 AND dat_b0_ecke.InvE3=1;"))

#-------------------------------------------------------------------------------
# 4. Zustand BWI3: Baumtabelle mit Klassifizierungsmerkmalen

ges1 = sqlQuery(channel,paste("SELECT dat_b0_ecke.Tnr, dat_b0_ecke.Enr,
                                  dat_b3_baeume.Bnr, dat_b3_baeume.Av, dat_b3_baeume.Bs, dat_b3_baeume.N_ha, dat_b3_baeume.StfM, dat_b3_baeume.VolR,
                                  xyk_x_ba.Zu_BaBWI AS BaGr, xyk_x_ba.ba_ln,
                                  xyk1_y_AlKl20.ICode AS AlKl,
                                  xyk1_y_BhdSt10.ICode AS BhdSt
                                FROM (((((((dat_b0_ecke INNER JOIN dat_b3_ecke_w ON (dat_b0_ecke.Enr = dat_b3_ecke_w.Enr) AND (dat_b0_ecke.Tnr = dat_b3_ecke_w.Tnr))
                                  INNER JOIN dat_b3_baeume ON (dat_b0_ecke.Enr = dat_b3_baeume.Enr) AND (dat_b0_ecke.Tnr = dat_b3_baeume.Tnr))
                                  INNER JOIN xyk_x_ba ON dat_b3_baeume.Ba = xyk_x_ba.ICode)
                                  INNER JOIN xyk_y_BhdSt5 ON dat_b3_baeume.Bhdst5 = xyk_y_BhdSt5.ICode)
                                  INNER JOIN xyk1_y_BhdSt10 ON xyk_y_BhdSt5.ZU_BhdSt10 = xyk1_y_BhdSt10.ICode)
                                  INNER JOIN xyk_y_AlKl5 ON dat_b3_baeume.Alkl5 = xyk_y_AlKl5.ICode)
                                  INNER JOIN xyk1_y_AlKl20 ON xyk_y_AlKl5.ZU_AlKl20 = xyk1_y_AlKl20.ICode)
                                WHERE dat_b3_ecke_w.Wa=5 AND dat_b3_ecke_w.Begehbar=1
                                  AND dat_b0_ecke.InvE3=1
                                  AND dat_b3_baeume.Bz=1;"))

#ges1$id=paste(ges1$Tnr,ges1$Enr,sep="")

b3_baeume=ges1

#-------------------------------------------------------------------------------
# 5. Zustand BWI2: Baumtabelle mit Klassifizierungsmerkmalen

ges1 = sqlQuery(channel,paste("SELECT dat_b0_ecke.Tnr, dat_b0_ecke.Enr,
                                  dat_b2_baeume.Bnr, dat_b2_baeume.Av, dat_b2_baeume.Bs, dat_b2_baeume.N_ha, dat_b2_baeume.StfM, dat_b2_baeume.VolR,
                                  xyk_x_ba.Zu_BaBWI AS BaGr, xyk_x_ba.ba_ln,
                                  xyk1_y_AlKl20.ICode AS AlKl,
                                  xyk1_y_BhdSt10.ICode AS BhdSt
                                FROM (((((((dat_b0_ecke INNER JOIN dat_b2_ecke_w ON (dat_b0_ecke.Enr = dat_b2_ecke_w.Enr) AND (dat_b0_ecke.Tnr = dat_b2_ecke_w.Tnr))
                                  INNER JOIN dat_b2_baeume ON (dat_b0_ecke.Enr = dat_b2_baeume.Enr) AND (dat_b0_ecke.Tnr = dat_b2_baeume.Tnr))
                                  INNER JOIN xyk_x_ba ON dat_b2_baeume.Ba = xyk_x_ba.ICode)
                                  INNER JOIN xyk_y_BhdSt5 ON dat_b2_baeume.Bhdst5 = xyk_y_BhdSt5.ICode)
                                  INNER JOIN xyk1_y_BhdSt10 ON xyk_y_BhdSt5.ZU_BhdSt10 = xyk1_y_BhdSt10.ICode)
                                  INNER JOIN xyk_y_AlKl5 ON dat_b2_baeume.Alkl5 = xyk_y_AlKl5.ICode)
                                  INNER JOIN xyk1_y_AlKl20 ON xyk_y_AlKl5.ZU_AlKl20 = xyk1_y_AlKl20.ICode)
                                WHERE dat_b2_ecke_w.Wa<3 AND dat_b2_ecke_w.Begehbar=1
                                  AND dat_b0_ecke.InvE2=1;"))


#ges1$id=paste(ges1$Tnr,ges1$Enr,sep="")

b2_baeume=ges1

#-------------------------------------------------------------------------------
# 6. Vereinigungsmenge B�ume f�r Zuwachs/Nutzung mit Klassifizierungsmerkmalen

ges1 = sqlQuery(channel,paste("SELECT dat_b0_ecke.Tnr, dat_b0_ecke.Enr,
                                  dat_b23_baeume_m_s.Bnr, dat_b23_baeume_m_s.Pk, dat_b23_baeume_m_s.Bs, dat_b23_baeume_m_s.N_ha, dat_b23_baeume_m_s.VolE, dat_b23_baeume_m_s.VolR, dat_b23_baeume_m_s.VolRDiff,
                                  xyk_x_ba.Zu_BaBWI AS BaGr, xyk_x_ba.ba_ln,
                                  xyk1_y_AlKl20.ICode AS AlKl,
                                  xyk1_y_BhdSt10.ICode AS BhdSt,
                                  dat_b23_tab_w.VegJahre, dat_b23_tab_w.KalJahre
                                FROM ((((((((dat_b0_ecke INNER JOIN dat_b3_ecke_w ON (dat_b0_ecke.Enr = dat_b3_ecke_w.Enr) AND (dat_b0_ecke.Tnr = dat_b3_ecke_w.Tnr))
                                  INNER JOIN dat_b23_tab_w ON (dat_b0_ecke.VBl = dat_b23_tab_w.Vbl) AND (dat_b0_ecke.Tnr = dat_b23_tab_w.Tnr))
                                  INNER JOIN dat_b23_baeume_m_s ON (dat_b0_ecke.Enr = dat_b23_baeume_m_s.Enr) AND (dat_b0_ecke.Tnr = dat_b23_baeume_m_s.Tnr))
                                  INNER JOIN xyk_x_ba ON dat_b23_baeume_m_s.Ba = xyk_x_ba.ICode)
                                  INNER JOIN xyk_y_BhdSt5 ON dat_b23_baeume_m_s.Bhdst5 = xyk_y_BhdSt5.ICode)
                                  INNER JOIN xyk1_y_BhdSt10 ON xyk_y_BhdSt5.ZU_BhdSt10 = xyk1_y_BhdSt10.ICode)
                                  INNER JOIN xyk_y_AlKl5 ON dat_b23_baeume_m_s.Alkl5 = xyk_y_AlKl5.ICode)
                                  INNER JOIN xyk1_y_AlKl20 ON xyk_y_AlKl5.ZU_AlKl20 = xyk1_y_AlKl20.ICode)
                                WHERE dat_b0_ecke.Use23=100;"))

#ges1$id=paste(ges1$Tnr,ges1$Enr,sep="")

# hier schon Zuwachs und Nutzung zuweisen
ges1$Zu_Zuwachs=ifelse((ges1$Pk>=0 & ges1$Pk<6) | ges1$Pk==9, 1, 0)
ges1$Zu_Nutzung=ifelse(ges1$Pk==2 | ges1$Pk==3 | ges1$Pk==9, 1, 0)
ges1$Zu_Abgang= ifelse((ges1$Pk>1 & ges1$Pk<6) | ges1$Pk==9, 1, 0)

b23_baeume=ges1

#-------------------------------------------------------------------------------
# 7. Zustand BWI3: Waldecken mit Klassifizierungsmerkmalen

ges1 = sqlQuery(channel,paste("SELECT dat_b0_ecke.Tnr, dat_b0_ecke.Enr, dat_b0_ecke.Bl, dat_b0_ecke.VBl, dat_b0_ecke.Use23,
                                xyk1_x_Wald3.ICode AS Wa,
                                xyk_x_BegehbarGr1.ICode AS Begehbar,
                                xyk1_x_EgK3.ICode AS Eg,
                                xyk1_y_EgGrklK.ICode AS EgGrkl,
                                xyk_x_NeGr1.ICode AS Ne
                              FROM (((((((((((dat_b0_ecke INNER JOIN dat_b3_ecke_w ON (dat_b0_ecke.Tnr = dat_b3_ecke_w.Tnr) AND (dat_b0_ecke.Enr = dat_b3_ecke_w.Enr))
                                INNER JOIN dat_b3_ecke_raum ON (dat_b0_ecke.Tnr = dat_b3_ecke_raum.Tnr) AND (dat_b0_ecke.Enr = dat_b3_ecke_raum.Enr))
                                INNER JOIN xyk_x_eg ON dat_b3_ecke_w.Eg = xyk_x_eg.ICode)
                                INNER JOIN xyk1_x_EgK3 ON xyk_x_eg.Zu_EgK3 = xyk1_x_EgK3.ICode)
                                INNER JOIN xyk_x_wa ON dat_b3_ecke_w.Wa = xyk_x_wa.ICode)
                                INNER JOIN xyk_x_begehbar ON dat_b3_ecke_w.Begehbar = xyk_x_begehbar.ICode)
                                INNER JOIN xyk_x_BegehbarGr1 ON xyk_x_begehbar.ZU_BegehbarGr1 = xyk_x_BegehbarGr1.ICode)
                                INNER JOIN xyk_x_eggrkl ON dat_b3_ecke_w.EgGrkl = xyk_x_eggrkl.ICode)
                                INNER JOIN xyk1_y_EgGrklK ON xyk_x_eggrkl.ZU_EgGrklK = xyk1_y_EgGrklK.ICode)
                                INNER JOIN xyk_x_ne ON dat_b3_ecke_w.Ne = xyk_x_ne.ICode)
                                INNER JOIN xyk_x_NeGr1 ON xyk_x_ne.ZU_NeGr1 = xyk_x_NeGr1.ICode)
                                INNER JOIN xyk1_x_Wald3 ON xyk_x_wa.Zu_Wald3 = xyk1_x_Wald3.ICode
                              WHERE dat_b0_ecke.InvE3=1;"))

#ges1$id=paste(ges1$Tnr,ges1$Enr,sep="")

b3_ecke=ges1

#-------------------------------------------------------------------------------
# 8. Zustand BWI2: Waldecken mit Klassifizierungsmerkmalen

ges1 = sqlQuery(channel,paste("SELECT dat_b0_ecke.Tnr, dat_b0_ecke.Enr, dat_b0_ecke.Bl, dat_b0_ecke.VBl, dat_b0_ecke.Use23,
                                xyk1_x_Wald3.ICode AS Wa,
                                xyk_x_BegehbarGr1.ICode AS Begehbar,
                                xyk1_x_EgK3.ICode AS Eg,
                                xyk1_y_EgGrklK.ICode AS EgGrkl,
                                xyk_x_NeGr1.ICode AS Ne
                              FROM (((((((((((dat_b0_ecke INNER JOIN dat_b2_ecke_w ON (dat_b0_ecke.Tnr = dat_b2_ecke_w.Tnr) AND (dat_b0_ecke.Enr = dat_b2_ecke_w.Enr))
                                INNER JOIN dat_b2_ecke_raum ON (dat_b0_ecke.Tnr = dat_b2_ecke_raum.Tnr) AND (dat_b0_ecke.Enr = dat_b2_ecke_raum.Enr))
                                INNER JOIN xyk_x_eg ON dat_b2_ecke_w.Eg = xyk_x_eg.ICode)
                                INNER JOIN xyk1_x_EgK3 ON xyk_x_eg.Zu_EgK3 = xyk1_x_EgK3.ICode)
                                INNER JOIN xyk_x_wa ON dat_b2_ecke_w.Wa = xyk_x_wa.ICode)
                                INNER JOIN xyk_x_begehbar ON dat_b2_ecke_w.Begehbar = xyk_x_begehbar.ICode)
                                INNER JOIN xyk_x_BegehbarGr1 ON xyk_x_begehbar.ZU_BegehbarGr1 = xyk_x_BegehbarGr1.ICode)
                                INNER JOIN xyk_x_eggrkl ON dat_b2_ecke_w.EgGrkl = xyk_x_eggrkl.ICode)
                                INNER JOIN xyk1_y_EgGrklK ON xyk_x_eggrkl.ZU_EgGrklK = xyk1_y_EgGrklK.ICode)
                                INNER JOIN xyk_x_ne ON dat_b2_ecke_w.Ne = xyk_x_ne.ICode)
                                INNER JOIN xyk_x_NeGr1 ON xyk_x_ne.ZU_NeGr1 = xyk_x_NeGr1.ICode)
                                INNER JOIN xyk1_x_Wald3 ON xyk_x_wa.Zu_Wald3 = xyk1_x_Wald3.ICode
                              WHERE dat_b0_ecke.InvE2=1
                                AND dat_b2_ecke_w.Wa<>6;"))

#ges1$id=paste(ges1$Tnr,ges1$Enr,sep="")

b2_ecke=ges1

#-------------------------------------------------------------------------------
# Vereinigungsmenge Waldecken

#-------------------------------------------------------------------------------
# Totholz, Bestockung,

#-------------------------------------------------------------------------------
# Trakte f�r Sonderauswertungen (Wuchsregionen, Gro�landschaften etc.)
# Fl�chen der VBl wie bei regionaler Auswertung (alt) �ber Anzahl Ecken bestimmen. 
# Feld Bl kennzeichnet Region, nicht mehr Bundesland

#-------------------------------------------------------------------------------

close(channel)

save("b0_ecke_z", "b0_ecke_v", "b3_baeume", "b2_baeume", "b23_baeume", "b3_ecke", "b2_ecke", "woa", file=pfadaus)