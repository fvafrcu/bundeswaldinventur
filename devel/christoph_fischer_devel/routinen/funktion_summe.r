# Funktion zur Berechnung von Summenwerten
# max 2fache Klassifizierung, alternativ auch Gesamtwerte ohne Klassifizierung

summe=function(bl,       # Bundesland, Codierung gem�� BWI, kann mehrere enthalten. Zb. (1:6)
               b0_ecke,  # Tabelle mit Trakten (Wald und Nichtwald). b0_ecke_z bei Zustand, b0_ecke_z bei Ver�nderung
               b3_ecke,  # Waldecken, muss besetzt sein. b3_ecke, b2_ecke
							 b3_baum,  # Attributtabelle (Baumdaten, Totholz, Verj�ngung, etc), kann NA bei reeller Waldfl�che sein 
               y,        # Zielmerkmal, muss Bezeichnung in Attributtabelle entsprechen. Immer F bei reeller Waldfl�che
               merkmal1, # 1. Klassifizierungsmerkmal: Bl, BaGr, Eg, Akl, BHD, Wa ....
               merkmal2, # 2. Klassifizierungsmerkmal: analog, aber nur wenn merkmal1 besetzt ist. Sonst NA.
							 csv,      # zus�tzlich Ausgabe als csv? 0,1
							 excel)		 # zus�tzlich Ausgabe als Exceltabelle? 0,1
{

# Selektieren der Traktecken
  ges=b0_ecke[b0_ecke$Bl %in% bl,]				# auszuwertende Bl				
	vbl=unique(ges$VBl)											# auszuwertende VBl
	eck1=b3_ecke[b3_ecke$VBl %in% vbl,]			# Waldecken der auszuwertenden VBl

# verschneiden der Waldecken mit Attributtabelle
# Zielmerkmal deklarieren und hochrechnen
	if(exists("b3_baum"))
	{
		eck1=merge(eck1, b3_baum, by=c("Tnr", "Enr"))
		eck1$y=eck1[,y]*eck1$N_ha

		if(y=="StfM")
	  {
	    eck1=eck1[eck1$Bs==1,]
	    eck1$y=eck1$y/10000
		}
	}

# Sonderfall reelle Waldfl�che (y=F)
  if(y=="F")
  {
    eck1$y=1
  }

# Schl�sseltabelle Klassifizierungsmerkmale
	xtab=get(load("tab/x_tab.RData"))
	
# Bezeichnungen und Hilfstabellen f�r Ausgabe
	if(csv==1 | excel==1)
	{
		namaus=paste(y, merkmal1, merkmal2, Sys.Date(), sep="_")
		namaus=gsub("-","", namaus)
		namaus=gsub("NA_","", namaus)

# Hilfstabelle f�r Ausgabe
		ytab=read.table("tab/y_tab.csv", header=T, sep=";")
		ytab=ytab[ytab$y_daten==y & y$zustand==1,]
	}
	
#-------------------------------------------------------------
# Hochrechnung ohne Klassifizierung
  if(is.na(merkmal1))
  {

# Traktsummen
  	eck1$y_trakt=ave(eck1$y, eck1$VBl, eck1$Tnr, FUN=sum)
    eck1=unique(eck1[, c("VBl", "Tnr","y_trakt")])

# Outer Join mit allen Trakten, y=0 f�r unbesetzte Stichproben
    ges=merge(ges, eck1, by=c("Tnr", "VBl"), all.x=T)
    ges[is.na(ges)]=0
    
# Mittel je VBL
		ges$y_mean_vbl=ave(ges$y_trakt/ges$n_ecken_vbl, ges$VBl, FUN=sum)

# Varianzen, komponentenweise berechnen
    ges$v3=ave((ges$y_trakt-ges$y_mean_vbl*ges$n_ecken_trakt)**2, ges$VBl, FUN=sum) 	# quadrierter Term (3. Term)
    ges$v2=ave(ges$Tnr, ges$VBl, FUN=length) 	   																			# Anzahl Trakte	(2. Term)
    ges$v1=1/ges$n_ecken_vbl**2 																											# Anzahl Ecken (1. Term)
    
    ges$var_y_mean_vbl=ges$v1*(ges$v2/(ges$v2-1))*ges$v3

# Zusammenrechnen der Straten    
		ges=unique(ges[,c("VBl", "Flaeche", "y_mean_vbl", "var_y_mean_vbl")]) 
    ges$y_ges=sum(ges$y_mean_vbl*ges$Flaeche) 
    ges$var_y_ges=sum(ges$var_y_mean_vbl*ges$Flaeche**2)
    ges$stdf=sqrt(ges$var_y_ges)/(ges$y_ges)

    ges$y_ges=round(ges$y_ges)
    ges$stdf=round(ges$stdf,2)
    
# Ausgabeformat
		ges=unique(ges[,c("y_ges", "stdf")])
	  names(ges)=c(y_aus, "Standardfehler") 
  }

#-------------------------------------------------------------
# Hochrechnung einfach klassifiziert
  if(!is.na(merkmal1) & is.na(merkmal2))
  {
    eck1$merkmal1=eck1[,merkmal1]

# Merkmalsstufen, alle vorkommenden Auspr�gungen
    stufe1=sort(unique(eck1$merkmal1))

# Kombination mit allen Trakten + Merkmalsstufen, f�r Ber�cksichtigung unbesetzter Stichproben
    ges=merge(ges,data.frame(merkmal1=stufe1))

# Traktsummen
  	eck1$y_trakt=ave(eck1$y, eck1$VBl, eck1$merkmal1, eck1$Tnr, FUN=sum)
    eck1=unique(eck1[, c("VBl", "merkmal1", "Tnr","y_trakt")])

# Outer Join mit allen Trakten, y=0 f�r unbesetzte Stichproben
    ges=merge(ges, eck1, by=c("Tnr", "VBl", "merkmal1"), all.x=T)
    ges[is.na(ges)]=0    

# Mittel je VBL
		ges$y_mean_vbl=ave(ges$y_trakt/ges$n_ecken_vbl, ges$VBl, ges$merkmal1, FUN=sum)    
    
# Varianzen, komponentenweise berechnen
    ges$v3=ave((ges$y_trakt-ges$y_mean_vbl*ges$n_ecken_trakt)**2, ges$VBl,ges$merkmal1, FUN=sum) 	# quadrierter Term (3. Term)
    ges$v2=ave(ges$Tnr, ges$VBl, FUN=length) 	   																									# Anzahl Trakte	(2. Term)
    ges$v1=1/ges$n_ecken_vbl**2 																																	# Anzahl Ecken (1. Term)
    
    ges$var_y_mean_vbl=ges$v1*(ges$v2/(ges$v2-1))*ges$v3
		
# Zusammenrechnen der Straten    
		ges=unique(ges[,c("VBl", "Flaeche", "merkmal1", "y_mean_vbl", "var_y_mean_vbl")]) 
    ges$y_ges=ave(ges$y_mean_vbl*ges$Flaeche, ges$merkmal1, FUN=sum) 
    ges$var_y_ges=ave(ges$var_y_mean_vbl*ges$Flaeche**2, ges$merkmal1, FUN=sum)
    ges$stdf=sqrt(ges$var_y_ges)/(ges$y_ges)

    ges$y_ges=round(ges$y_ges)
	  ges$stdf=round(ges$stdf,2)

# Ausgabeformat
		xtab1=xtab[xtab$Merkmal==merkmal1,]
	  ges=merge(ges, xtab1, by.x="merkmal1", by.y="ICode")
		ges=unique(ges[,c("Stufe", "y_ges", "stdf")])

	  names(ges)=c(merkmal1, y_aus, "Standardfehler") 
  
  }

#-------------------------------------------------------------
# Hochrechnung zweifach klassifiziert
 if(!is.na(merkmal2))
  {
 		eck1$merkmal1=eck1[,merkmal1]
    eck1$merkmal2=eck1[,merkmal2]

# Merkmalsstufen, alle vorkommenden Auspr�gungen
    stufe1=sort(unique(eck1$merkmal1))
		stufe2=sort(unique(eck1$merkmal2))

# Kombination mit allen Trakten + Merkmalsstufen, f�r Ber�cksichtigung unbesetzter Stichproben
    ges=merge(ges,merge(data.frame(merkmal1=stufe1),data.frame(merkmal2=stufe2)))

# Traktsummen
  	eck1$y_trakt=ave(eck1$y, eck1$VBl, eck1$merkmal1, eck1$merkmal2, eck1$Tnr, FUN=sum)
    eck1=unique(eck1[, c("VBl", "merkmal1", "merkmal2", "Tnr","y_trakt")])

# Outer Join mit allen Trakten, y=0 f�r unbesetzte Stichproben
    ges=merge(ges, eck1, by=c("Tnr", "VBl", "merkmal1", "merkmal2"), all.x=T)
    ges[is.na(ges)]=0    

# Mittel je VBL
		ges$y_mean_vbl=ave(ges$y_trakt/ges$n_ecken_vbl, ges$VBl, ges$merkmal1, ges$merkmal2, FUN=sum)    
    
# Varianzen, komponentenweise berechnen
    ges$v3=ave((ges$y_trakt-ges$y_mean_vbl*ges$n_ecken_trakt)**2, ges$VBl, ges$merkmal1, ges$merkmal2, FUN=sum) 	# quadrierter Term (3. Term)
    ges$v2=ave(ges$Tnr, ges$VBl, FUN=length) 	   																																	# Anzahl Trakte	(2. Term)
    ges$v1=1/ges$n_ecken_vbl**2 																																									# Anzahl Ecken (1. Term)
    
    ges$var_y_mean_vbl=ges$v1*(ges$v2/(ges$v2-1))*ges$v3
		
# Zusammenrechnen der Straten    
		ges=unique(ges[,c("VBl", "Flaeche", "merkmal1", "merkmal2", "y_mean_vbl", "var_y_mean_vbl")]) 
    ges$y_ges=ave(ges$y_mean_vbl*ges$Flaeche, ges$merkmal1, ges$merkmal2, FUN=sum) 
    ges$var_y_ges=ave(ges$var_y_mean_vbl*ges$Flaeche**2, ges$merkmal1, ges$merkmal2, FUN=sum)
    ges$stdf=sqrt(ges$var_y_ges)/(ges$y_ges)

    ges$y_ges=round(ges$y_ges)
    ges$stdf=round(ges$stdf,2)
 
# Ausgabeformat
		xtab1=xtab[xtab$Merkmal==merkmal1,]
		xtab2=xtab[xtab$Merkmal==merkmal2,]
	  ges=merge(ges, xtab1, by.x="merkmal1", by.y="ICode")
		ges=merge(ges, xtab2, by.x="merkmal2", by.y="ICode")
		ges=ges[order(ges$merkmal1, ges$merkmal2),]
	  ges=unique(ges[,c("Stufe.x", "Stufe.y", "y_ges", "stdf")])

	  names(ges)=c(merkmal1, merkmal2, y_aus, "Standardfehler") 
    
  }
  


# Ausgabe als csv
	write.table(ges, paste("ausgabe/",nam,".csv", sep=""), sep=",", dec=".", quote=F, row.names=F)
	
# optional Ausgabe als Exceltabelle
	if(excel==1)
	{
		library(xlsx) 
		source("routinen/funktion_ausgabe_excel.r")
		ausgabe_excel(ges=ges, merkmal1=merkmal1, merkmal2=merkmal2, y_aus=y_aus, nam=nam)
	}
	
	return(ges)
}
