# erstellt Exceltabelle mit Beschreibung
# optionale Ausgabe bei Hochrechnung


ausgabe_excel=function(ges, merkmal1, merkmal2, ytab, namaus)
{

	fileout=paste("ausgabe/",namaus,".xlsx", sep="")
	fehler="Standardfehler relativ"
	y_aus=paste(ytab$y_name, " [", ytab$y_einheit, "]", ", ", ytab$bezug, sep="")

	
	
# ohne Klassifizierung
	if(is.na(merkmal1) & is.na(merkmal2))
	{
    ret=data.frame(a1=NA, ay=ges[,1])
    names(ret)=c("","")

# Fehler
    feh=data.frame(a1=NA, ay=ges[,2])
    names(feh)=c("","")
	}	
	
# einfach klassifiziert
	if(!is.na(merkmal1) & is.na(merkmal2))
	{

		df=data.frame(a1=ges[,1], a2=NA, ay=ges[,2])
		ret=reshape(df, idvar="a2",timevar="a1",direction="wide")
    names(ret)=gsub("ay.","",names(ret))
    names(ret)=c("",names(ret)[2:length(names(ret))])

# Fehler
		df=data.frame(a1=ges[,1], a2=NA, ay=ges[,3])
    feh=reshape(df, idvar="a2",timevar="a1",direction="wide")
    names(feh)=gsub("ay.","",names(feh))
    names(feh)=c("",names(feh)[2:length(names(feh))])	
	}
	
# zweifach klassifiziert
	if(!is.na(merkmal2))
	{

		df=data.frame(a1=ges[,1] ,a2=ges[,2], ay=ges[,3])
    ret=reshape(df, idvar="a2",timevar="a1",direction="wide")
    names(ret)=gsub("ay.","",names(ret))
    names(ret)=c("",names(ret)[2:length(names(ret))])

# Fehler
		df=data.frame(a1=ges[,1] ,a2=ges[,2], ay=ges[,4])
    feh=reshape(df, idvar="a2",timevar="a1",direction="wide")
    names(feh)=gsub("ay.","",names(feh))
    names(feh)=c("",names(feh)[2:length(names(feh))])
    
	}

# Ausgabe als Excel, Fehlertabelle neben Datentabelle
    wb = createWorkbook(type="xlsx")
      sheet  = createSheet(wb, sheetName=namaus)
      addDataFrame(y_aus, sheet, startRow=1, startColumn=1, row.names=F, col.names=F)
      addDataFrame(ret, sheet, startRow=2, startColumn=2, row.names=F)
      addDataFrame(feh, sheet, startRow=2, startColumn=5+dim(ret)[2], row.names=F)
      addDataFrame(fehler, sheet, startRow=1, startColumn=4+dim(ret)[2], row.names=F,col.names=F)
    saveWorkbook(wb,fileout)
}