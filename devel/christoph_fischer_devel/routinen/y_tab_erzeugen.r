# y-Tabelle. F�r Bezeichnung der Variablen in Ausgabetabellen
# Bezeichnungen (y_aus) k�nnen frei vergeben werden, Schl�sselfeld (y) muss Variablenbezeichnung in Rohdaten entsprechen
# bei Ratio zusammengesetzte Bezeichnung

# Ausgabe f�r workspace
pfadaus="tab/y_tab.RData"

ytab=data.frame(y=c(NA, "VolR", "VolE", "StfM"),
							  y_aus=c("Flaeche [ha]","Vorrat [Vfm]", "Vorrat [Efm]", "Flaeche [ha]"),
								stringsAsFactors=F)
	

save("ytab",  file=pfadaus)