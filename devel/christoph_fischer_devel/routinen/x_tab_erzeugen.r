# erzeugt Schlüsseltabellen für Ausgabe
# Übersetzung der numerischen Klassifizierungsmerkmale
# Tabellen werden als workspace abgelegt
# Stand BWI3, muss für jede Inventur neu angepasst werden
# bisher nur ODBC- Schnittstelle

#-------------------------------------------------------------------------------
# Pfad und DB-Anbindung
library(RODBC)
pfad="db/bwi_1014.mdb"
channel=odbcConnectAccess(pfad)

# Ausgabe für workspace
pfadaus="tab/x_tab.RData"

#-------------------------------------------------------------------------------
# Bundesland
ges1 = sqlQuery(channel,paste("SELECT xyk_x_bl.ICode,
                                      xyk_x_bl.KurzD AS Stufe
                               FROM xyk_x_bl;"))
ges1=ges1[ges1$ICode>0,]
ges1$Merkmal="Bl"
x_bl=ges1


# Waldart
ges1 = sqlQuery(channel,paste("SELECT xyk1_x_Wald3.ICode,
                                      xyk1_x_Wald3.KurzD AS Stufe
                               FROM xyk1_x_Wald3
                               WHERE xyk1_x_Wald3.ICode<>7 ;"))
ges1=ges1[ges1$ICode>0,]
ges1$Merkmal="Wa"
x_wa=ges1


# Eigentumsart
ges1 = sqlQuery(channel,paste("SELECT xyk1_x_EgK3.ICode,
                                      xyk1_x_EgK3.LangD AS Stufe
                               FROM xyk1_x_EgK3;"))
ges1=ges1[ges1$ICode>0,]
ges1$Merkmal="Eg"
x_eg=ges1


# Eigentumsgrößenklasse
ges1 = sqlQuery(channel,paste("SELECT xyk1_y_EgGrklK.ICode,
                                      xyk1_y_EgGrklK.KurzD AS Stufe
                               FROM xyk1_y_EgGrklK;"))

ges1=ges1[ges1$ICode>0,]
ges1$Merkmal="EgGrkl"
x_eggrkl=ges1


# Begehbarkeit
ges1 = sqlQuery(channel,paste("SELECT xyk_x_BegehbarGr1.ICode,
                                      xyk_x_BegehbarGr1.KurzD AS Stufe
                               FROM xyk_x_BegehbarGr1;"))
ges1=ges1[ges1$ICode>0,]
ges1$Merkmal="Begehbar"
x_begehbar=ges1


# Nutzungseinschränkung
ges1 = sqlQuery(channel,paste("SELECT xyk_x_NeGr1.ICode,
                                      xyk_x_NeGr1.KurzD AS Stufe
                               FROM xyk_x_NeGr1;"))
ges1=ges1[ges1$ICode>0,]
ges1$Merkmal="Ne"
x_ne=ges1


# Bhd-Stufen
ges1 = sqlQuery(channel,paste("SELECT xyk1_y_BhdSt10.ICode,
                                      xyk1_y_BhdSt10.LangD AS Stufe
                               FROM xyk1_y_BhdSt10;"))
ges1=ges1[ges1$ICode>0,]
ges1$Merkmal="BhdSt"
x_bhd=ges1


# Alterklassen
ges1 = sqlQuery(channel,paste("SELECT xyk1_y_AlKl20.ICode,
                                      xyk1_y_AlKl20.LangD AS Stufe
                               FROM xyk1_y_AlKl20;"))
ges1=ges1[ges1$ICode>0,]
ges1$Merkmal="AlKl"
x_akl=ges1


# Baumartengruppen
ges1 = sqlQuery(channel,paste("SELECT DISTINCT xyk_x_ba.Zu_BaBWI AS ICode,
                                      xyk_x_ba.BaGr AS Stufe
                               FROM xyk_x_ba;"))

ges1=ges1[ges1$ICode>0,]
ges1$Merkmal="BaGr"
x_ba=ges1
x_ba=x_ba[x_ba$ICode>0 & !is.na(x_ba$ICode),]

#-------------------------------------------------------------------------------
close(channel)
xtab=rbind(x_bl,x_wa, x_eg, x_eggrkl, x_begehbar, x_ne, x_bhd, x_akl, x_ba)
xtab$Stufe=gsub(" ", "", xtab$Stufe)

save("xtab",  file=pfadaus)

