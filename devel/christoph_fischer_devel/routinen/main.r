# Einstellungen f�r BWI 3- Auswertung �ber Auswahldialoge festlegen

library(svDialogs)
library(RODBC)

# Pfad der Eingangsdaten
pfad=dlgOpen(default = getwd(),title = "Select Database", filters = dlgFilters[c("All"), ])$res
if(length(pfad)==0)
  {
    print("--- Fehler: keine Datenquelle ausgew�hlt")
  }

#----------------------------------------------------------------------------------------------
# Bundesl�nder w�hlen

channel<-odbcConnectAccess(pfad)
bl_ausw <- sqlQuery(channel,paste("SELECT ICODE, LangD
                                    FROM xyk_x_bl"), as.is=T)
close(channel)

# Liste mit Bundesl�ndern
bl_list=c(bl_ausw$LangD)

bl=(dlgList(bl_list,multiple=T, title="Bundesland ausw�hlen")$res)


if(length(bl)==0)
  {
    print("--- Fehler: kein Bundesland ausgew�hlt")
  }


# Bundesland in Codierung �bersetzen
bl=bl_ausw[bl_ausw$LangD %in% bl,]$ICODE


# regionale Auswertung, wenn nur ein Bundesland ausgew�hl wurde?
region=0
if(length(bl)==1)
  {
    bl_list=c("ja", "nein")
    bl=(dlgList(bl_list,multiple=T, title="regionale Auswertung?")$res)
    if(bl==1) {region=1}
  }
  

#----------------------------------------------------------------------------------------------
# Zielmerkmal ausw�hlen
zm_list=c("Waldfl�che",
          "Baumartenfl�che",
          "Vorrat",
          "Vorrat je ha",
          "Zuwachs",
          "Abgang",
          "Nutzung",
          "Totholz")






dlgDir(default = getwd(), title="Datenbank w�hlen")

dlgOpen(default = getwd(), title, multiple = FALSE)

wann=strsplit(date()," ", fixed=T)

