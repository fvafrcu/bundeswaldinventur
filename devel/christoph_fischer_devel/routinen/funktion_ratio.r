# Funktion zur Berechnung von Ratios
# nur Zustand
# max 2fache Klassifizierung, alternativ auch ohne Klassifizierung

ratio=function(bl,       # Bundesland, Codierung gem�� BWI, kann mehrere enthalten. Zb. (1:6)
               b0_ecke,  # Tabelle mit Trakten (Wald und Nichtwald). b0_ecke_z bei Zustand, b0_ecke_z bei Ver�nderung
               b3_ecke,  # Waldecken, muss besetzt sein. b3_ecke, b2_ecke
							 b3_att,   # Attributtabelle (Baumdaten, Totholz, Verj�ngung, etc), muss vorhanden sein
               y,        # Z�hler im Ratio. Muss Bezeichnung in Attributtabelle entsprechen. VolR, VolRDiff, VolE
							 x,        # Nenner im Ratio. StflM bei ideellem Bezug (Vorrat/ha), Fl bei reellem Fl�chenbezug
               merkmal1, # 1. Klassifizierungsmerkmal: Bl, BaGr, Eg, Akl, BHD, Wa ....
               merkmal2, # 2. Klassifizierungsmerkmal: analog, aber nur wenn merkmal1 besetzt ist. NA bei einfach klassifizierter Hochrechnung
							 csv,      # zus�tzlich Ausgabe als csv? 0,1
							 excel)		 # zus�tzlich Ausgabe als Exceltabelle? 0,1
{

# Selektieren der Traktecken
  ges=b0_ecke[b0_ecke$Bl %in% bl,]				# auszuwertende Bl				
	vbl=unique(ges$VBl)											# auszuwertende VBl
	eck1=b3_ecke[b3_ecke$VBl %in% vbl,]			# Waldecken der auszuwertenden VBl

# Stratengewichte f�r Varianz
	w=unique(ges[,c("VBl", "Flaeche")])
	w$wh=w$Flaeche/sum(w$Flaeche)
	w$Flaeche=NULL
	
# verschneiden der Waldecken mit Attributtabelle
# Zielmerkmal deklarieren
	if(x=="StfM")
	{
		eck1=merge(eck1, b3_att, by=c("Tnr", "Enr"))
		eck1$y=eck1[,y]*eck1$N_ha
	  eck1$x=eck1[,x]*eck1$N_ha/10000
	  eck1=eck1[eck1$Bs==1,]
	}

# Sonderfall reeller Fl�chenbezug. Trakteckensumme x=1.
  if(x=="Fl")
  {
# merken f�r Traktsumme x, reell
  	eck1_save=eck1[eck1$Wa!=4, c("VBl", "Tnr", "Enr")]
    eck1_save$x_trakt=ave(eck1_save$Enr, eck1_save$VBl, eck1_save$Tnr, FUN=length)
    eck1_save=unique(eck1_save[,c("VBl", "Tnr", "x_trakt")])
    
  	eck1=merge(eck1, b3_att,  by=c("Tnr", "Enr"))
  	
  	eck1$x=1
  	eck1$y=eck1[,y]*eck1$N_ha
  }
	

# Schl�sseltabelle Klassifizierungsmerkmale
	xtab=get(load("tab/x_tab.RData"))
	

# Bezeichnungen und Hilfstabellen f�r csv und Excel Ausgabe
	if(csv==1 | excel==1)
	{
		namaus=paste(y,x, merkmal1, merkmal2, Sys.Date(), sep="_")
		
		if(x=="Fl")
		{
			namaus=paste(y, "Flaeche", merkmal1, merkmal2, Sys.Date(), sep="_")
		}
		
		namaus=gsub("-","", namaus)
		namaus=gsub("NA_","", namaus)

		ytab=read.table("tab/y_tab.csv", header=T, sep=";")
		ytab=ytab[ytab$y_daten==y & ytab$y_funktion=="ratio" & ytab$zustand==1,]
	}

#-------------------------------------------------------------
# Hochrechnung ohne Klassifizierung
  if(is.na(merkmal1))
  {

# Traktsummen
  	eck1$y_trakt=ave(eck1$y, eck1$VBl, eck1$Tnr, FUN=sum)
    eck1$x_trakt=ave(eck1$x, eck1$VBl, eck1$Tnr, FUN=sum)
  	eck1=unique(eck1[, c("VBl", "Tnr","y_trakt", "x_trakt")])


# Outer Join mit allen Trakten, y=0 f�r unbesetzte Stichproben
    ges=merge(ges, eck1, by=c("Tnr", "VBl"), all.x=T)
  
# bei reellem Bezug   
    if(x==1)
	  {
			ges$x_trakt=NULL
    	ges=merge(ges, eck1_save, by=c("VBl", "Tnr"), all.x=T)
    }
    
    ges[is.na(ges)]=0
    
# Mittel je VBL
		ges$y_mean_vbl=ave(ges$y_trakt/ges$n_ecken_vbl, ges$VBl, FUN=sum)
		ges$x_mean_vbl=ave(ges$x_trakt/ges$n_ecken_vbl, ges$VBl, FUN=sum)

# Ratio je VBL
		ges$yx_vbl=ges$y_mean_vbl/ges$x_mean_vbl
		
# Varianzen Z�hler und Nenner, komponentenweise berechnen (Dahm, Gleichung 2.3)
    ges$v3=ave((ges$y_trakt-ges$y_mean_vbl*ges$n_ecken_trakt)**2, ges$VBl, FUN=sum) 		# quadrierter Term, y, (3. Term)
    ges$v2=ave(ges$Tnr, ges$VBl, FUN=length) 	   																			  # Anzahl Trakte	(2. Term)
    ges$v1=1/ges$n_ecken_vbl**2 																											  # Anzahl Ecken (1. Term)
    ges$var_y_mean_vbl=ges$v1*(ges$v2/(ges$v2-1))*ges$v3

    ges$v3=ave((ges$x_trakt-ges$x_mean_vbl*ges$n_ecken_trakt)**2, ges$VBl, FUN=sum) 		# quadrierter Term, x,  (3. Term)
    ges$var_x_mean_vbl=ges$v1*(ges$v2/(ges$v2-1))*ges$v3

# Kovarianz je VBl (Dahm, Gleichung 3.7), notwendig bei mehreren VBl
    ges$v1=1/ges$n_ecken_vbl**2 
    ges$v2=ave(ges$Tnr, ges$VBl, FUN=length)
		ges$v3=ave((ges$x_trakt-ges$x_mean_vbl*ges$n_ecken_trakt)*(ges$y_trakt-ges$y_mean_vbl*ges$n_ecken_trakt), ges$VBl, FUN=sum)
		ges$covar_yx_vbl=ges$v1*(ges$v2/(ges$v2-1))*ges$v3

# a) Varianz Ratio je VBl (Dahm, Gleichung 2.7) = Varianzsch�tzer bei nur einem VBl
    if(length(vbl)==1){
	    ges$v3=ave((ges$y_trakt-ges$yx_vbl*ges$x_trakt)**2, ges$VBl, FUN=sum)								# quadrierter Term, y, (3. Term)
	    ges$v2=ave(ges$Tnr, ges$VBl, FUN=length)                                            # Anzahl Trakte	(2. Term)
	    ges$v1=ave(ges$x_trakt, ges$VBl, FUN=sum) 																					# Summe x je VBl
	    
	    ges$yx=ges$yx_vbl
	    ges$var_yx=(1/ges$v1**2)*(ges$v2/(ges$v2-1))*ges$v3
    }

# b) Zusammenrechnen der Straten bei mehreren VBl (Dahm, Gleichung 3.7) 
		if(length(vbl)>1){   
 			
			ges=unique(ges[,c("VBl", "Flaeche", "y_mean_vbl", "x_mean_vbl", "var_y_mean_vbl", "var_x_mean_vbl", "covar_yx_vbl")]) 
			ges=merge(ges, w)
 			ges$yx=sum(ges$y_mean_vbl*ges$Flaeche)/sum(ges$x_mean_vbl*ges$Flaeche)  
 			ges$x_mean=sum(ges$x_mean_vbl*ges$w) 
 			ges$var_yx=sum((1/ges$x_mean**2)*ges$wh**2*(ges$var_y_mean_vbl+ges$yx**2*ges$var_x_mean_vbl-2*ges$yx*ges$covar_yx_vbl))
		}	

    ges$stdf=sqrt(ges$var_yx)/(ges$yx)
    ges$yx=round(ges$yx)
    ges$stdf=round(ges$stdf,2)
    
# Ausgabeformat
		ges=unique(ges[,c("yx", "stdf")])
	  #names(ges)=c(y_aus, "Standardfehler") 
  }

#-------------------------------------------------------------
# Hochrechnung einfach klassifiziert
  if(!is.na(merkmal1) & is.na(merkmal2))
  {
    eck1$merkmal1=eck1[,merkmal1]

# Merkmalsstufen, alle vorkommenden Auspr�gungen
    stufe1=sort(unique(eck1$merkmal1))

# Kombination mit allen Trakten + Merkmalsstufen, f�r Ber�cksichtigung unbesetzter Stichproben
    ges=merge(ges,data.frame(merkmal1=stufe1))

# Traktsummen
  	eck1$y_trakt=ave(eck1$y, eck1$VBl, eck1$merkmal1, eck1$Tnr, FUN=sum)
    eck1$x_trakt=ave(eck1$x, eck1$VBl, eck1$merkmal1, eck1$Tnr, FUN=sum)
		eck1=unique(eck1[, c("VBl", "merkmal1", "Tnr", "y_trakt", "x_trakt")])    


# Outer Join mit allen Trakten, y=0 f�r unbesetzte Stichproben
    ges=merge(ges, eck1, by=c("Tnr", "VBl", "merkmal1"), all.x=T)

# bei reellem Bezug   
    if(x==1)
	  {
			ges$x_trakt=NULL
    	ges=merge(ges, eck1_save, by=c("VBl", "Tnr"), all.x=T)
    }
    
    ges[is.na(ges)]=0
    
# Mittel je VBL
		ges$y_mean_vbl=ave(ges$y_trakt/ges$n_ecken_vbl, ges$VBl, ges$merkmal1, FUN=sum)
		ges$x_mean_vbl=ave(ges$x_trakt/ges$n_ecken_vbl, ges$VBl, ges$merkmal1, FUN=sum)

# Ratio je VBL
		ges$yx_vbl=ges$y_mean_vbl/ges$x_mean_vbl
		
# Varianzen Z�hler und Nenner, komponentenweise berechnen (Dahm, Gleichung 2.3)
    ges$v3=ave((ges$y_trakt-ges$y_mean_vbl*ges$n_ecken_trakt)**2, ges$VBl, ges$merkmal1, FUN=sum) 		# quadrierter Term, y, (3. Term)
    ges$v2=ave(ges$Tnr, ges$VBl, FUN=length) 	   																			  							# Anzahl Trakte	(2. Term)
    ges$v1=1/ges$n_ecken_vbl**2 																											  							# Anzahl Ecken (1. Term)
    ges$var_y_mean_vbl=ges$v1*(ges$v2/(ges$v2-1))*ges$v3

    ges$v3=ave((ges$x_trakt-ges$x_mean_vbl*ges$n_ecken_trakt)**2, ges$VBl, ges$merkmal1, FUN=sum)			# quadrierter Term, x,  (3. Term)
    ges$var_x_mean_vbl=ges$v1*(ges$v2/(ges$v2-1))*ges$v3

# Kovarianz je VBl (Dahm, Gleichung 3.7), notwendig bei mehreren VBl
    ges$v1=1/ges$n_ecken_vbl**2 
    ges$v2=ave(ges$Tnr, ges$VBl, FUN=length)
		ges$v3=ave((ges$x_trakt-ges$x_mean_vbl*ges$n_ecken_trakt)*(ges$y_trakt-ges$y_mean_vbl*ges$n_ecken_trakt), 
			ges$VBl, ges$merkmal1, FUN=sum)
		ges$covar_yx_vbl=ges$v1*(ges$v2/(ges$v2-1))*ges$v3

# a) Varianz Ratio je VBl (Dahm, Gleichung 2.7) = Varianzsch�tzer bei nur einem VBl
    if(length(vbl)==1){
	    ges$v3=ave((ges$y_trakt-ges$yx_vbl*ges$x_trakt)**2, ges$VBl,ges$merkmal1, FUN=sum)	# quadrierter Term, y, (3. Term)
	    ges$v2=ave(ges$Tnr, ges$VBl, FUN=length)                                            # Anzahl Trakte	(2. Term)
	    ges$v1=ave(ges$x_trakt, ges$VBl, ges$merkmal1, FUN=sum) 														# Summe x je VBl
	    
	    ges$yx=ges$yx_vbl
	    ges$var_yx=(1/ges$v1**2)*(ges$v2/(ges$v2-1))*ges$v3
    }

# b) Zusammenrechnen der Straten bei mehreren VBl (Dahm, Gleichung 3.7) 
		if(length(vbl)>1){   
 			
			ges=unique(ges[,c("VBl", "Flaeche", "merkmal1", "y_mean_vbl", "x_mean_vbl", "var_y_mean_vbl", "var_x_mean_vbl", "covar_yx_vbl")]) 
			ges=merge(ges,w)
 			ges$yx=ave(ges$y_mean_vbl*ges$Flaeche, ges$merkmal1, FUN=sum)/ave(ges$x_mean_vbl*ges$Flaeche, ges$merkmal1, FUN=sum)

 			ges$x_mean=ave(ges$x_mean_vbl*ges$w, ges$merkmal1, FUN=sum) 
 			
 			ges$var_yx=ave((1/ges$x_mean**2)*ges$wh**2*(ges$var_y_mean_vbl+ges$yx**2*ges$var_x_mean_vbl-2*ges$yx*ges$covar_yx_vbl), 
 				ges$merkmal1, FUN=sum)
		}	

    ges$stdf=sqrt(ges$var_yx)/(ges$yx)

    ges$yx=round(ges$yx)
    ges$stdf=round(ges$stdf,2)
    
# Ausgabeformat
		xtab1=xtab[xtab$Merkmal==merkmal1,]
	  ges=merge(ges, xtab1, by.x="merkmal1", by.y="ICode")
		ges=unique(ges[,c("Stufe", "yx", "stdf")])

	  #names(ges)=c(merkmal1, y_aus, "Standardfehler") 
	  
  }

#-------------------------------------------------------------
# Hochrechnung zweifach klassifiziert
 if(!is.na(merkmal2))
  {
 		eck1$merkmal1=eck1[,merkmal1]
    eck1$merkmal2=eck1[,merkmal2]

# Merkmalsstufen, alle vorkommenden Auspr�gungen
    stufe1=sort(unique(eck1$merkmal1))
		stufe2=sort(unique(eck1$merkmal2))

# Kombination mit allen Trakten + Merkmalsstufen, f�r Ber�cksichtigung unbesetzter Stichproben
    ges=merge(ges,merge(data.frame(merkmal1=stufe1),data.frame(merkmal2=stufe2)))

# Traktsummen
  	eck1$y_trakt=ave(eck1$y, eck1$VBl, eck1$merkmal1, eck1$merkmal2, eck1$Tnr, FUN=sum)
    eck1$x_trakt=ave(eck1$x, eck1$VBl, eck1$merkmal1, eck1$merkmal2, eck1$Tnr, FUN=sum)
  	eck1=unique(eck1[, c("VBl", "merkmal1", "merkmal2", "Tnr","y_trakt", "x_trakt")])

# Outer Join mit allen Trakten, y=0 f�r unbesetzte Stichproben
    ges=merge(ges, eck1, by=c("Tnr", "VBl", "merkmal1", "merkmal2"), all.x=T)
    
# bei reellem Bezug   
    if(x==1)
	  {
			ges$x_trakt=NULL
    	ges=merge(ges, eck1_save, by=c("VBl", "Tnr"), all.x=T)
    }
    
    ges[is.na(ges)]=0
    
# Mittel je VBL
		ges$y_mean_vbl=ave(ges$y_trakt/ges$n_ecken_vbl, ges$VBl, ges$merkmal1, ges$merkmal2, FUN=sum)
		ges$x_mean_vbl=ave(ges$x_trakt/ges$n_ecken_vbl, ges$VBl, ges$merkmal1, ges$merkmal2, FUN=sum)

# Ratio je VBL
		ges$yx_vbl=ges$y_mean_vbl/ges$x_mean_vbl
		
# Varianzen Z�hler und Nenner, komponentenweise berechnen (Dahm, Gleichung 2.3)
    ges$v3=ave((ges$y_trakt-ges$y_mean_vbl*ges$n_ecken_trakt)**2, ges$VBl, ges$merkmal1, ges$merkmal2, FUN=sum) 		# quadrierter Term, y, (3. Term)
    ges$v2=ave(ges$Tnr, ges$VBl, FUN=length) 	   																			  										        # Anzahl Trakte	(2. Term)
    ges$v1=1/ges$n_ecken_vbl**2 																											  							              # Anzahl Ecken (1. Term)
    ges$var_y_mean_vbl=ges$v1*(ges$v2/(ges$v2-1))*ges$v3

    ges$v3=ave((ges$x_trakt-ges$x_mean_vbl*ges$n_ecken_trakt)**2, ges$VBl, ges$merkmal1, ges$merkmal2, FUN=sum)			# quadrierter Term, x,  (3. Term)
    ges$var_x_mean_vbl=ges$v1*(ges$v2/(ges$v2-1))*ges$v3

# Kovarianz je VBl (Dahm, Gleichung 3.7), notwendig bei mehreren VBl
    ges$v1=1/ges$n_ecken_vbl**2 
    ges$v2=ave(ges$Tnr, ges$VBl, FUN=length)
		ges$v3=ave((ges$x_trakt-ges$x_mean_vbl*ges$n_ecken_trakt)*(ges$y_trakt-ges$y_mean_vbl*ges$n_ecken_trakt), 
			ges$VBl, ges$merkmal1, ges$merkmal2, FUN=sum)
		ges$covar_yx_vbl=ges$v1*(ges$v2/(ges$v2-1))*ges$v3

# a) Varianz Ratio je VBl (Dahm, Gleichung 2.7) = Varianzsch�tzer bei nur einem VBl
    if(length(vbl)==1){
	    ges$v3=ave((ges$y_trakt-ges$yx_vbl*ges$x_trakt)**2, ges$VBl, ges$merkmal1, ges$merkmal2, FUN=sum)	# quadrierter Term, y, (3. Term)
	    ges$v2=ave(ges$Tnr, ges$VBl, FUN=length)                                                          # Anzahl Trakte	(2. Term)
	    ges$v1=ave(ges$x_trakt, ges$VBl, ges$merkmal1, ges$merkmal2, FUN=sum) 														              # Summe x je VBl
	    
	    ges$yx=ges$yx_vbl
	    ges$var_yx=(1/ges$v1**2)*(ges$v2/(ges$v2-1))*ges$v3
    }

# b) Zusammenrechnen der Straten bei mehreren VBl (Dahm, Gleichung 3.7) 
		if(length(vbl)>1){   
 			
			ges=unique(ges[,c("VBl", "Flaeche", "merkmal1", "merkmal2", "y_mean_vbl", "x_mean_vbl", "var_y_mean_vbl", "var_x_mean_vbl", "covar_yx_vbl")]) 
			ges=merge(ges,w)
 			ges$yx=ave(ges$y_mean_vbl*ges$Flaeche, ges$merkmal1, ges$merkmal2, FUN=sum)/
 				ave(ges$x_mean_vbl*ges$Flaeche, ges$merkmal1, ges$merkmal2, FUN=sum)

 			ges$x_mean=ave(ges$x_mean_vbl*ges$w, ges$merkmal1, ges$merkmal2, FUN=sum) 
 			
 			ges$var_yx=ave((1/ges$x_mean**2)*ges$wh**2*(ges$var_y_mean_vbl+ges$yx**2*ges$var_x_mean_vbl-2*ges$yx*ges$covar_yx_vbl), 
 				ges$merkmal1, ges$merkmal2, FUN=sum)
 			
		}	

    ges$stdf=sqrt(ges$var_yx)/(ges$yx)

    ges$yx=round(ges$yx)
    ges$stdf=round(ges$stdf,2)
 
# Ausgabeformat
		xtab1=xtab[xtab$Merkmal==merkmal1,]
		xtab2=xtab[xtab$Merkmal==merkmal2,]
	  ges=merge(ges, xtab1, by.x="merkmal1", by.y="ICode")
		ges=merge(ges, xtab2, by.x="merkmal2", by.y="ICode")
		ges=ges[order(ges$merkmal1, ges$merkmal2),]
	  ges=unique(ges[,c("Stufe.x", "Stufe.y", "yx", "stdf")])

	  #names(ges)=c(merkmal1, merkmal2, y_aus, "Standardfehler") 
    
  }

	
# Ausgabe als csv
	if(csv==1)
	{
		write.table(ges, paste("ausgabe/",nam,".csv", sep=""), sep=",", dec=".", quote=F, row.names=F)
	}

	# optional Ausgabe als Exceltabelle
	if(excel==1)
	{
		library(xlsx) 
		source("routinen/funktion_ausgabe_excel.r")
		ausgabe_excel(ges=ges, merkmal1=merkmal1, merkmal2=merkmal2, y_tab=y_tab, namaus=namaus)
	}
	

	return(ges)
}
