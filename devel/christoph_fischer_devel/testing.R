y <- "VolR"
x <- "StfM"
nha <- "N_ha"
enr <- "Enr"
tnr <- "Tnr"
#% from auswertung.R
load("tab/tabellen.RData")
example_tnr <- 10591 
bl=8; 
b0_ecke=b0_ecke_z;
b3_att=b3_baeume;
b3_ecke=b3_ecke;




#%  from routinen/funktion_ratio_z.r
# Selektieren der Traktecken
  ges=b0_ecke[b0_ecke$Bl %in% bl,]				# auszuwertende Bl				
	vbl=unique(ges$VBl)											# auszuwertende VBl
	eck1=b3_ecke[b3_ecke$VBl %in% vbl,]			# Waldecken der auszuwertenden VBl

# Stratengewichte für Varianz
	w=unique(ges[,c("VBl", "Flaeche")])
	w$wh=w$Flaeche/sum(w$Flaeche)
	w$Flaeche=NULL
	
# verschneiden der Waldecken mit Attributtabelle
# Zielmerkmal deklarieren
	if(x=="StfM")
	{
		eck1=merge(eck1, b3_att, by=c("Tnr", "Enr"))
		eck1$y=eck1[,y]*eck1$N_ha
	  eck1$x=eck1[,x]*eck1$N_ha/10000
	  eck1=eck1[eck1$Bs==1,]
	}


baeume <- eck1


subset(baeume, 
       Tnr == example_tnr, select = c("Tnr", "Enr", "Bnr", "N_ha", "StfM", "VolR"))
subset(bundeswaldinventur::get_data("baeume.3", package = "bwibw"), 
       TNr == example_tnr, select = c("TNr", "ENr", "BNr", "NHa2", "StFl2", "VolV2"))
###########
subset(baeume, 
       Tnr == example_tnr & Enr == 2 & Bnr == 6, select = c("Tnr", "Enr", "Bnr", "N_ha", "StfM", "VolR"))
subset(bundeswaldinventur::get_data("baeume.3", package = "bwibw"), 
       TNr == example_tnr & ENr == 2 & BNr == 6, select = c("TNr", "ENr", "BNr", "NHa2", "StFl2", "VolV2"))
colSums(subset(bundeswaldinventur::get_data("baeume.3", package = "bwibw"), 
       TNr == example_tnr & ENr == 2, select = c("TNr", "ENr", "BNr", "NHa2", "StFl2", "VolV2"))
)
if (FALSE) {
bund <-  cuutils::read_ms_access_table("../bwi20150320_alle_daten2012.mdb", "b3_baeume")
subset(bund, 
       tnr == example_tnr & enr == 2 & bnr == 6, select = c("tnr", "enr", "bnr", "n_ha", "stf", "stfm", "stfm_hb", "volr"))
t <- subset(bund, 
       tnr == example_tnr & enr == 2 , select = c("tnr", "enr", "bnr", "n_ha", "stf", "stfm", "stfm_hb", "volr"))
sum(t$n_ha * t$stfm)
warning("StfM und StFl2 sind voellig verschieden!, beim Bund gibt es StfM, Stf und StfM_Hb!")
warning("stfm ist ein baummerkmal und muss erst noch mit nha gewichtet werden, stfl2 hat das schon. das ist eher kacke")
}
###########
subset(baeume, Tnr == example_tnr)
tey <- aggregate(x = baeume[[y]] * baeume[[nha]], 
                     by = list(enr = baeume[[enr]], tnr = baeume[[tnr]]), FUN = sum)
tex <- aggregate(x = baeume[[x]] * baeume[[nha]] / 10000, 
                     by = list(enr = baeume[[enr]], tnr = baeume[[tnr]]), FUN = sum)

te <- merge(tex, tey, by = c("enr", "tnr"))
names(te) <- sub("^x\\.", "", names(te))



# Traktsummen
  	eck1$y_trakt=ave(eck1$y, eck1$VBl, eck1$Tnr, FUN=sum)
    eck1$x_trakt=ave(eck1$x, eck1$VBl, eck1$Tnr, FUN=sum)
  	eck1=unique(eck1[, c("VBl", "Tnr","y_trakt", "x_trakt")])


tx <- aggregate(te[["x"]],
                by = list(tnr = te[["tnr"]]), FUN = sum)

ty <- aggregate(te[["y"]],
                by = list(tnr = te[["tnr"]]), FUN = sum)

t <- merge(x = tx, y = ty, by = "tnr") # Beware of the ordering (therefor using named arguemnts)!
t[["y"]] <- t[["x.y"]] / t[["x.x"]]
subset(eck1, Tnr == example_tnr)
subset(te, tnr == example_tnr)
colSums(subset(te, tnr == example_tnr)[TRUE, c("x", "y")])
subset(t, tnr == example_tnr)


# Outer Join mit allen Trakten, y=0 für unbesetzte Stichproben
    ges=merge(ges, eck1, by=c("Tnr", "VBl"), all.x=T)
  
# bei reellem Bezug   
    if(x==1)
	  {
			ges$x_trakt=NULL
    	ges=merge(ges, eck1_save, by=c("VBl", "Tnr"), all.x=T)
    }
    
    ges[is.na(ges)]=0
    
# Mittel je VBL
		ges$y_mean_vbl=ave(ges$y_trakt/ges$n_ecken_vbl, ges$VBl, FUN=sum)
		ges$x_mean_vbl=ave(ges$x_trakt/ges$n_ecken_vbl, ges$VBl, FUN=sum)

# Ratio je VBL
		ges$yx_vbl=ges$y_mean_vbl/ges$x_mean_vbl

subset(ges, Tnr == example_tnr)
print(cf <- unique(ges[,c("yx_vbl")]))

subset(t, tnr == example_tnr)
print(dc <- sum(t[["x.y"]])  / sum(t[["x.x"]]))

if ( isTRUE(all.equal(cf, dc))) {
    message("Congrats, worked.")
} else {
    stop("bugger that.")
}

