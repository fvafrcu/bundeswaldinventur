pkgload::load_all()
set_options(data_source = "bwibw", name = "bundeswaldinventur")
f <- FVBN.bagrupp.akl.dkl.stratum.fun.2e
result <- f(baeume = get_data("baeume.3"),
            ecken = get_data("ecken.3"),
            trakte = get_data("trakte.3"),
            A = get_design("a", 3),
            inv = 2,
            BA.grupp = get_bwi_species_groups("bc"),
            A.klass = list(A.ob = 500, A.b = 100), 
            D.klass = list(D.unt = 0, D.ob = 50, D.b = 25, Ndh = TRUE),
            auswahl = list(Wa = c(3, 5), Begehbar = 1),
            is_name_arrays = TRUE
            )
absolute_stats <- result[["T.FVBN.Bagr.Akl.Dkl"]]
absolute_stats["BAF", "Wert", "Alle BA" , , ]
relative_stats <- result[["FVBN.ha.Bagr.Akl.Dkl"]]
relative_stats["BA_Proz", "Wert", "Alle BA" , , ]
