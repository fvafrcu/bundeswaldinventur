#!/bin/sh
# Well, this is quick and and dirty. 
# And it's just a little helper script, it's not part of the package.
# I can't keep the R CMD stuff, and I'm used  to 'make', so I wrap R CMDs into
# make.
temp_file=$(tempfile)
TAB="$(printf '\t')"
DOLLAR=$
rpackage=$(basename $(pwd)) 
rpversion=$(grep 'Version:' DESCRIPTION |cut -f2 -d' ')

test_changes_file=tmp_test_change.R


cat > $test_changes_file <<\EOF
git_status <- system("git status --porcelain", intern = TRUE)
modified_codes <- basename(sub(" M ", "", grep(" M R/", git_status , value = TRUE)))
if (length(modified_codes) > 0) {
    pattern <- paste0("\\.*", sub("\\.R", "", modified_codes), collapse = '|')
    devtools::test(filter = pattern)
} else {
  message("no code files with modified git status found.")
}
EOF

cat > Makefile <<EOF
# header
rpackage=${rpackage}
rpversion=${rpversion}
roxy_code=tmp_roxy.r
temp_file=${temp_file}
test_changes_file=${test_changes_file}
EOF

cat >> Makefile <<\EOF
# devtools
dev_all: dev_test dev_check
dev: dev_test_change dev_check
dev_test:
	rm ${temp_file} || TRUE; \
	Rscript --vanilla -e 'devtools::test()' >  ${temp_file} 2>&1; \
	sed -n -e '/^DONE.*/q;p' < ${temp_file} > dev_test.Rout 
dev_test_change:
	rm ${temp_file} || TRUE; \
	Rscript --vanilla ${test_changes_file} >  ${temp_file} 2>&1; \
	sed -n -e '/^DONE.*/q;p' < ${temp_file} > dev_test_change.Rout 
dev_check:
	rm ${temp_file} || TRUE; \
	Rscript --vanilla -e 'devtools::check()' > ${temp_file} 2>&1; \
	grep -v ".*'/" ${temp_file} | grep -v ".*/tmp/R.*" > dev_check.Rout 
EOF

cat >> Makefile <<\EOF
# R CMD 
craninstall: crancheck
	R --vanilla CMD INSTALL  ${rpackage}_${rpversion}.tar.gz
crancheck: check 
	export _R_CHECK_FORCE_SUGGESTS_=FALSE && \
        R CMD check --as-cran ${rpackage}_${rpversion}.tar.gz 
install: check 
	R --vanilla CMD INSTALL  ${rpackage}_${rpversion}.tar.gz && \
        printf '===== have you run\n\tmake check_demo && ' && \
        printf 'make package_tools && make runit && make coldr\n?!\n' 
install_bare: build_bare 
	R --vanilla CMD INSTALL  ${rpackage}_${rpversion}.tar.gz 
check_bare: build_bare 
	export _R_CHECK_FORCE_SUGGESTS_=FALSE && \
        R --vanilla CMD check --no-examples ${rpackage}_${rpversion}.tar.gz && \
        printf '===== run\n\tmake install\n!!\n'
check: build 
	export _R_CHECK_FORCE_SUGGESTS_=FALSE && \
        R --vanilla CMD check ${rpackage}_${rpversion}.tar.gz && \
        printf '===== run\n\tmake install\n!!\n'
build_bare: 
	R --vanilla CMD build ../${rpackage}
build: roxy 
	R --vanilla CMD build ../${rpackage}
direct_check:  
	R --vanilla CMD check ../${rpackage} ## check without build -- not recommended
roxy:
	rm man/* || true
	printf "devtools::load_all()\n" > ${roxy_code}
	printf "roxygen2::roxygenize('.', roclets = c('rd'))\n" >> ${roxy_code}
	R --vanilla CMD BATCH --vanilla ${roxy_code}

EOF
