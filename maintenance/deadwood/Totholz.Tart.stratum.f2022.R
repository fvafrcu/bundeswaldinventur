#-------------------------------------------------------------------------------
Totholz.Tart.stratum.f2022 <-
          function(totholz,ecken,trakte,bwi,krit,tart.grupp,A,auswahl)

#-------------------------------------------------------------------------------
#Funktion wertet nach  Totholzart <Tart>, Aufnahmekriterium <krit> [2,3]
#im Stratum <auswahl> aus. Bezüglich
#der Totholz-Aufnahmeschwellen gibt es 2 Varianten:
#(1) die BWI 2-Kriterien mit Schwellendurchmessern: 20 cm am schwächeren Ende
#bei liegenden Stücken  bzw. BHD sowie 60 cm Schnittflächendurchmesser bei Stücken
#(2) die BWI 3-Kriterien mit Schwellendurchmessern: 10 cm am schwächeren Ende
#bei liegenden Stücken  bzw. BHD  20 cm Schnittflächendurchmesser bei Stücken.
#BWI 2-Kriterien bei BWI 3-Aufnahme:
#Wenn(([Tart]=4 Und ([Tbd]>=60 Oder [Tl]>=0,5)) Oder ([Tart]<>4 Und [Tbd]>=20)
#<tart.grupp> ist eine Liste, mit der Th-Arten-Gruppen definiert werden können
#z. B. tart.grupp = list(g1=c(11,12,13),g2=c(2,3),g3=c(4,5))
#Wird für tart.grupp NA übergeben, werden die Original-tart-Kategorien verwendet,
#also bei BWI 3: 11,12,13,2,3,4,5; BWI 2: 1,2,3,4,5

#Verallgemeinerte Version für die Auswertung unterschiedlicher Inventurzeit-
#punkte, also BWI 2, BWI 3:
#daher werden die Datentabellen <totholz>, die Eckenmerkmale <ecken> und die
#<trakte> als Argumente übergeben.
#Die Tabelle <totholz> muss mindestens die Attribute
#Tnr, Enr, Nr, Tbagr, Tart, Tzg, Tbd, Tsd,  Tl, Tvol, Anz, Thf enthalten;
#Hinweis BWI 3: Durchmesser: Tbd Tsd; Länge: Tl; BWI 2: Dm, Lge, kein <Anz>
#---------------------
#Basis-Attribute (BWI 2)
#<TBAGr>, <TArt>, <TZg>, <TVol>, <Dm>, <Lge>, <THf>
#neu bei BWI 3
#<Tbd>: Durchmesser am stärkeren Ende bzw. BHD
#<Tsd>: Durchmesser am dünnen Ende (nur bei Tart 1 und 13),
#für Klassifikation relevant ist bei Bruchstücken der Mitten-Durchmesser,
#bei Stücken mit Wurzelanlauf (stehend oder liegend) ist es der BHD
#Mittendurchmesser berechnen, wenn Tsd > 0, aus Volumen und Länge
#<Dm> = sqrt(tvol/tl/pi)*200; die Bezeichnung <Dm> entspricht auch derjenigen
#der BWI 2;
#Wegen der Einheitlichkeit mit BWI 2wird der Bezeichner <tl> in <lge>
#umgewandelt.

#<bwi> (2 oder 3): um mit denselben Algorithmen für beide Zustände arbeiten zu
#können
#<A> ist die Fläche in ha des Inventurgebiets zum jeweiligen Inventurzeitpunkt
#(sollte eigentlich konstant sein)

#<auswahl> definiert das auszuwertende Stratum entsprechend Funktion <stratum.f2022>

#Version 1.0 (kä/17.02.2014)
#Hinweis: um Konflikte mit unterschiedlicher Groß-/Kleinschreibung bei den
#Attributnamen zu vermeiden, werden innerhalb dieser Funktion alle Attribut-
#Namen auf Kleinschreibung umgestellt
#-------------------------------------------------------------------------------
#Modifikationen 09.03.2020:
#Auswahl BWI-3-Aufnahme nach Kriterien BWI 2: bei Tart 11,12 (Stücke mit Wurzelanlauf)
#wurde SChwelle von 20 auf 17 abgesenkt, da diese Stücke am stärkeren Ende mindestens 
#20 cm haben m3ssen (lt. BWI-2-Kriterien) und eine D-ZUnahme von 3 cm gutachterlich
#unterstellt wird.
#-------------------------------------------------------------------------------
{
  stratum <- stratum.f2022(auswahl,ecken)
  #Kleinschreibung
  names(stratum) <- tolower(names(stratum))
  names(trakte)  <- tolower(names(trakte))
  n.te.s <- length(stratum[,1])
  y <- aggregate(rep(1,length(stratum[,1])),by=list(stratum$tnr),sum)
  names(y) <- c("tnr","y")
  n.t.s <- length(y[,1])
  #Teilmenge der Trakte im Auswertungsstratum
  y <- merge(y,subset(trakte,select=c(tnr,m),by=c(tnr)))
  #Alle Tratecken im Inventurgebiet
  x <- trakte$m
  #n Trakte im Inventurgebiet ist konstant
  nT <- length(trakte[,1])
  #----------------
  #HBFl. [ha]
  T.hbf <- sum(y$y)/sum(x)*A
  var.T.hbf <-  nT/(nT-1)*T.hbf^2*(sum(y$y^2)/sum(y$y)^2+sum(x^2)/sum(x)^2
                      -2*sum(y$y*y$m)/sum(y$y)/sum(x))
  #var.T.hbf^0.5
  se.T.hbf <- var.T.hbf^0.5 #Standardfehler
  #----------------
  #Kleinschreibung aller Atttributnahmen in <baeume>
  names(totholz) <- tolower(names(totholz))
  #Harmonisierung der Bezeichnungen zwischen BWI 2 und 3
  if(bwi==3) {
    #Bezeichnung <tl> durch <lge> (wie bei BWI 2 ersetzen
    names(totholz) <- sub("tl",names(totholz),replacement="lge")
    #Mittendurchmesser wird bei Bruchstücken (mit 2 Durchmessern als mittlerer
    #Walzendurchmesser ermittelt
    totholz$dm <- ifelse(totholz$tsd>0&!is.na(totholz$tsd),
              round(200*sqrt(totholz$tvol/totholz$lge/pi),1),totholz$tbd)
  }else #BWI 2
  {
    #Attribut <anz> einfügen
    totholz$anz <- rep(1,length(totholz[,1]))
  }
  #Auswahl nach Aufnahme-Kriterium:
  #nur sinnvoll, wenn Totholz-Aufnahme der BWI 3 vorliegt, dann kann entweder
  #nachden Totholfaufnahmekriterien der BWI 3 oder BWI 2 ausgewertet werden
  krit.bwi2 <- ifelse(bwi==3 & krit==2,T,F)

  if(krit.bwi2)
  #Ursprünglich: Wenn(([Tart]=4 Und ([Tbd]>=60 Oder [Tl]>=0,5)) Oder ([Tart]<>4 Und [Tbd]>=20)
  #kä/09.03.2020: bei der BWI 3 wird beim liegenden Totholz weiter differenziert:
  #11: liegend, ganzer Baum; 12: liegend, Stammanlauf; 13: liegend, Oberstück
  #bei 11 und 12 wird in 1.3 m Entfernung vom dickeren Ende der BHD gemessen
  #(Anmerkung: eigenlich m3sste die fiktive Stockhöhe berücksichtigt werden)
  #d.h. beim Vergleich mit der BWI 2 Totholzaufnahme, bei der diese Unterscheidung nicht
  #erfolgt ist, muss bei der Anpassung der BWI-3-Aufnahme an die BWI-2-Totholzaufnahme-
  #kriterien bei der Totholzart 11 und 12 die Schwelle nicht bei 20, sondern bei
  #einem kleineren D. erfolgen, da diese Stücke am dickeren Ende wegen des Wurzelanlaufs
  #stärker sind; die D-Zunahme zum dickeren Ende wird gutachterlich mit 3 cm angenommen,
  #d.h. bei TArt 11 und 12 werden Durchmesser bereits ab 17 cm als BWI-2-konform betrachtet,
  #da am dickeren Ende bei 3 cm Zunahme 20 cm erreicht sind
  #
  {
     #totholz <- subset(totholz,(tart!=4&tbd>=20)|(tart==4&(tbd>=60|lge>=0.5)))
     totholz <- subset(totholz, (tart%in%c(11,12)&tbd>=17)|(tart%in%c(13,2,3)&tbd>=20)
                       |(tart==4&(tbd>=60|lge>=0.5)))
  }

  #Attribute und Untermenge des Stratums aus <totholz> auswählen
  totholz.s <- merge(subset(totholz,select=c(tnr,enr,nr,tart,tvol,dm,lge,anz,thf)),
                    subset(stratum,select=c(tnr,enr)),by=c("tnr","enr"),all.y=T)
  if(is.na(tart.grupp[[1]][1]))
  {
    tart.lab <- unique(totholz$tart)
    tart.lab <- tart.lab[order(tart.lab)]
    tart.grupp=list()
    tart.k <- length(tart.lab)
    for (i in 1:tart.k){tart.grupp[[i]]=tart.lab[i]}
  }else
  {
    tart.k <- length(tart.grupp)
    #tart.lab <- "Gesamt"
    tart.lab <- NULL
    for(i in 1:tart.k) tart.lab[i] <- paste(tart.grupp[[i]],collapse = ",")
  }
  
  #Array für Ergebnisse (Totals und SE jeweils nach tbagr, tart, tzg)
  #Es gibt 2 Zielgrößen <Y>: V [m3], N (Gesamtzahl)
  #für diese 2 Größen werden jeweils der Gesamtwert ("Total") und der Stichproben-
  #fehler (SE) berechnet, und zwar jeweils für die 3 Baumartengruppen <tbagr>,
  #7 Totholzarten <tart>, 4 Zersetzungsgrade <tzg> sowie D.k Durchmesserklassen

  Y.tart    <- array(dim=c(2,2,tart.k),dimnames = list(c("V [m3]", "N (Gesamtzahl)"),
                                                       c("Wert","SE"),tart.lab))
  nT.tart   <- array(dim=c(tart.k,1))
  #Hektarbezogene Kennwerte. 2 Zielgrößen:  Th-Vol je ha, N Th je ha,
  Yha.tart  <- array(dim=c(2,2,tart.k),dimnames = list(c("V [m3/ha]", "N/ha"),
                                                       c("Wert","SE"),tart.lab))
  #----------------

  for (i in 1:tart.k)  #Totholzart
  {
    totholz.tart <- subset(totholz.s,tart%in%tart.grupp[[i]],
                        select=c(tnr,enr,tart,dm,lge,tvol,anz,thf))
    if (length(totholz.tart[,1])== 0)
    {
      Y.tart[1:2,1,i]   <- rep(0,2)  #Zielgröße Total
      Y.tart[1:2,2,i]   <- rep(0,2)  #Stichprobenfehler (SE)
      Yha.tart[1:2,1,i] <- rep(0,2)
      Yha.tart[1:2,1,i] <- rep(0,2)
      nT.tart[i,]    <- 0  #n PSU (Trakte)
    }else
    {
      #Nach Trakt aggregieren
      #Vol der BA-Gruppe [m3] als "v"
      xy <- aggregate(totholz.tart$tvol*totholz.tart$thf*totholz.tart$anz,
              by=list(totholz.tart$tnr),sum)
      names(xy) <- c("tnr","v")
      #Anzahl Totholzstücke als "n"
      xy <- cbind(xy,aggregate(totholz.tart$thf*totholz.tart$anz,
                                      by=list(totholz.tart$tnr),sum)$x)
      names(xy)[3] <- "n"
      #Hbf-Ecken (Bezugsfläche ha HB) und Anzahl Ecken je Trakt
      #hinzufügen (in <y> enthalten: y$y, y$m)
      xy <- merge(xy,y,by=c("tnr"),all.x=T)
      #Umbennen von xy$y in xy$x (Symbol für Bezugsfläche Holzboden)
      names(xy)[4] <- "x"
      #Anzahl Trakte (i.S. von PSU) im Teilkollektiv ijk
      nT.tart[i,] <- length(xy[,1])
      for (j in 1:2)
      {
        #Zielgrößen Y {Vol, N)
        #Total
        Y.tart[j,1,i] <-  sum(xy[,(1+j)])/sum(x)*A
        #Zugehöriger Stichprobenfehler
        Y.tart[j,2,i] <- Y.tart[j,1,i]*sqrt(
          nT/(nT-1)*(sum(xy[,(1+j)]^2)/sum(xy[,(1+j)])^2+sum(x^2)/sum(x)^2
                                -2*sum(xy[,(1+j)]*xy$m)/sum(xy[,(1+j)])/sum(x)))

        #Ratio-Schätzer (Th-Vol/ha, Th-N/ha)
        #Bezugsfläche ist die HBF des Stratums (also keine BA-Fläche!)
        Yha.tart[j,1,i] <- Y.tart[j,1,i]/T.hbf
        Yha.tart[j,2,i] <- Yha.tart[j,1,i]*sqrt(
          nT/(nT-1)*(sum(xy[,(1+j)]^2)/sum(xy[,(1+j)])^2+sum(xy$x^2)/sum(xy$x)^2
                             -2*sum(xy[,(1+j)]*xy$x)/sum(xy[,(1+j)])/sum(xy$x)))

      }#End for l (Zielgrößen)
    }#End if ... else
  }#End for i (Th-Art)
  #-----------------------
  #Tabelle für BA-Gruppen

  #Dokumentation der Grunddaten und Auswertungsdatum der HR
  a <- regexpr("/",totholz$bemerk[1],fixed=T)
  b <- nchar(as.character(totholz$bemerk[1]))
  version.totholz.b  <- substr(as.character(totholz$bemerk[1]),a,b)
  Log <- list(Datum=Sys.time(),
      Version.totholz.b=substr(as.character(totholz$bemerk[1]),a,b))

  return(list(Log=Log, Stratum=auswahl, nTE=n.te.s, HBF=T.hbf, se.HBF=se.T.hbf,
              TArt.Liste=tart.lab,
              T.ThVN.Tart=Y.tart,
              ThVN.ha.Tart=Yha.tart,
              nT.Tart=nT.tart))
}#End <Totholz.Tart.stratum.f2022>
