After installation, please read the help page:

```r
help("bundeswaldinventur-package", package = "bundeswaldinventur")
```

```
## Analysing the Bundeswaldinventur in the State of Baden-Wuerttemberg
## 
## Description:
## 
##      A collection of design-based estimators and reporting functions
##      for substrata of the area of interest.
```

