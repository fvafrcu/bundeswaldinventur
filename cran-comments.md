Dear CRAN Team,
this is a resubmission of package 'bundeswaldinventur'. I have added the following changes:

* Add `get_game_damage()` as new version of verbiss.bagrupp.fun(), trying to
  solve different game damage codes.

Please upload to CRAN.
Best, Andreas Dominik

# Package bundeswaldinventur 0.31.0

Reporting is done by packager version 1.15.2


## Test environments
- R Under development (unstable) (2025-01-27 r87652)
   Platform: x86_64-pc-linux-gnu
   Running under: Devuan GNU/Linux 5 (daedalus)
   0 errors | 3 warnings | 7 notes
- win-builder (devel)

## Local test results
- RUnit:
    bundeswaldinventur_unit_test - 15 test functions, 0 errors, 0 failures in 47 checks.
- testthat:
    [ FAIL 0 | WARN 32 | SKIP 0 | PASS 157 ]
- tinytest:
    All ok, 0 results (0ms)
- Coverage by covr:
    bundeswaldinventur Coverage: 28.07%

## Local meta results
- Cyclocomp:
     Exceeding maximum cyclomatic complexity of 10 for FVBN.bagrupp.akl.dkl.stratum.f2022.2f by 36.
     Exceeding maximum cyclomatic complexity of 10 for FVBN.bagrupp.akl.dkl.stratum.fun.2e by 34.
     Exceeding maximum cyclomatic complexity of 10 for FVBN.bagrupp.akl.dkl.stratum.f2022.2.vbl by 32.
     Exceeding maximum cyclomatic complexity of 10 for FVBN.bagrupp.akl.dkl.stratum.f2022.2f.a by 30.
     Exceeding maximum cyclomatic complexity of 10 for VB.A.bagrupp.akl.dkl.stratum.f2022.4 by 29.
     Exceeding maximum cyclomatic complexity of 10 for VB.A.bagrupp.akl.dkl.stratum.f2022.3 by 27.
     Exceeding maximum cyclomatic complexity of 10 for VB.A.bagrupp.akl.dkl.stratum.fun.3 by 25.
     Exceeding maximum cyclomatic complexity of 10 for Totholz.bagr.art.zg.stratum.fun by 22.
     Exceeding maximum cyclomatic complexity of 10 for FVBN.bagrupp.akl.dkl.stratum.fun.2a by 21.
     Exceeding maximum cyclomatic complexity of 10 for VB.A.bagrupp.akl.dkl.stratum.fun.2 by 21.
     Exceeding maximum cyclomatic complexity of 10 for FVBN.bagrupp.akl.dkl.stratum.fun.2d by 19.
     Exceeding maximum cyclomatic complexity of 10 for FVBN.bagrupp.akl.dkl.stratum.fun.2c by 18.
     Exceeding maximum cyclomatic complexity of 10 for FVBN.bagrupp.akl.dkl.stratum.fun.2b by 17.
     Exceeding maximum cyclomatic complexity of 10 for iVB.ew.bagrupp.akl.dkl.stratum.f2022.2g by 17.
     Exceeding maximum cyclomatic complexity of 10 for iVB.ew.bagrupp.akl.dkl.stratum.f2022.bwi12 by 16.
     Exceeding maximum cyclomatic complexity of 10 for iVB.ew.bagrupp.akl.dkl.stratum.fun.2 by 15.
     Exceeding maximum cyclomatic complexity of 10 for iVB.ew.bagrupp.akl.dkl.stratum.fun.2g by 15.
     Exceeding maximum cyclomatic complexity of 10 for iVB.ew.bagrupp.akl.dkl.stratum.fun.bwi12 by 14.
     Exceeding maximum cyclomatic complexity of 10 for Totholz.klass.stratum.fun by 9.
     Exceeding maximum cyclomatic complexity of 10 for predict by 8.
     Exceeding maximum cyclomatic complexity of 10 for get_game_damage by 7.
     Exceeding maximum cyclomatic complexity of 10 for Totholz.Tart.stratum.fun by 4.
     Exceeding maximum cyclomatic complexity of 10 for adapt_script by 2.
     Exceeding maximum cyclomatic complexity of 10 for stamm.merkmale.bagr.akl.fun by 2.
     Exceeding maximum cyclomatic complexity of 10 for fvbn.kreis.fun.1 by 1.
     Exceeding maximum cyclomatic complexity of 10 for fvbn.stratum.fun.1 by 1.
     Exceeding maximum cyclomatic complexity of 10 for fvbn.stratum.fun.2 by 1.
- lintr:
    found 10532 lints in 18737 lines of code (a ratio of 0.5621).
- cleanr:
    found 1030 dreadful things about your code.
- codetools::checkUsagePackage:
    found 819 issues.
- devtools::spell_check:
    found 1173 unkown words.
