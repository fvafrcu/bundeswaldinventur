After installation, please read the help page:
```{r, eval = FALSE}
help("bundeswaldinventur-package", package = "bundeswaldinventur")
```
```{r, echo = FALSE}
  # insert developement page
  help_file <- file.path("man", "bundeswaldinventur-package.Rd")
  captured <- gsub('_\b', '',  capture.output(tools:::Rd2txt(help_file) ))
  cat(captured, sep = "\n")
```

