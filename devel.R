pkgload::load_all()
devtools::document()
set_options(data_source = "bwibw", name = "bundeswaldinventur")
f <- FVBN.bagrupp.akl.dkl.stratum.fun.2e
result <- f(baeume = get_data("baeume.3", package = "bwibw"),
        ecken = get_data("ecken.3", package = "bwibw"),
        trakte = get_data("trakte.3", package = "bwibw"),
        A = get_design("a", 3),
        inv = 2,
        BA.grupp = get_bwi_species_groups("bc"),
        A.klass = list(A.ob=500, A.b=500), 
        D.klass = list(D.unt = 0, D.ob = 500, D.b = 500, Ndh = F), 
        auswahl = list(Wa = c(3, 5), Begehbar = 1)
        )
bwi3 <- bwidata::bwi3_hr_bw
result1 <- f(baeume = bwi3[["bwi_3_baeume_b"]],
        ecken = bwi3[["eckenmerkmale_bwi3"]],
        trakte = bwi3[["trakte_bwi3"]],
        A = get_design("a", 3),
        inv = 2,
        BA.grupp = get_bwi_species_groups("bc"),
        A.klass = list(A.ob=500, A.b=500), 
        D.klass = list(D.unt = 0, D.ob = 500, D.b = 500, Ndh = F), 
        auswahl = list(Wa = c(3, 5), Begehbar = 1)
        )
all.equal(clean_log(result1), clean_log(result))

f <- FVBN.bagrupp.akl.dkl.stratum.fun.2e
result <- f(baeume = get_data("baeume.3", package = "bwibw"),
        ecken = get_data("ecken.3", package = "bwibw"),
        trakte = get_data("trakte.3", package = "bwibw"),
        A = get_design("a", 3),
        inv = 2,
        BA.grupp = get_bwi_species_groups("bc"),
        A.klass = list(A.ob=500, A.b=500), 
        D.klass = list(D.unt = 0, D.ob = 500, D.b = 500, Ndh = F), 
        auswahl = list(Wa = c(3, 5), Begehbar = 1)
        )

str(result)


f <- iVB.ew.bagrupp.akl.dkl.stratum.fun.2g
result <- f(baeume.23 = get_data("baeume.23", package = "bwibw"),
            baeume.3 = get_data("baeume.3", package = "bwibw"),
            ecken.2 = get_data("ecken.2", package = "bwibw"),
            ecken.3 = get_data("ecken.3", package = "bwibw"),
            trakte.3 = get_data("trakte.3", package = "bwibw"),
            A = get_design("a", 3),
            BA.grupp = get_bwi_species_groups(),
            A.klass = list(A.ob=140, A.b=20), 
            D.klass = list(D.unt = 0, D.ob = 500, D.b = 500, Ndh = F), 
            auswahl = list(Wa = c(3, 5), Begehbar = 1)
            )

str(result)
res <- result$iVB.bagr.akl.dkl[grep("V_DhmR/ha/J", result$Attribute), 1, , , 1]
res <- result$iVB.bagr.akl.dkl[grep("oiB/ha/J", result$Attribute), 1, , , 1]
dimnames(res) <- list(result$BAGR,
                      result$AKL)
res <- as.data.frame(res)[TRUE,  - (dim(res)[2])]
res$id <- rownames(res)
molten <- reshape2::melt(res[1:7,])
library(ggplot2)
ggplot(data = molten, aes(variable, value, group = id, col = id )) + geom_line()




