#' Selektiert die zu einem Stratum gehoerenden Stichproben
#'
#' "Stratifikator"-Funktion: selektiert die zu einem Stratum (Befundeinheit,
#' Subdomaene, Subpopulation) gehoerenden Stichproben (Traktecken) anhand
#' verschiedener Merkmale, die in der Tabelle \code{ecken} zusammengefasst sind
#' die Tabelle \code{ecken} enthaelt nur die Stichproben im Wald (Holzboden,
#' Nichtholzboden, begehbar, nicht begehbar). Aufnahmen liegen natuerlich nur
#' fuer die Traktecken auf begehbarem Holzboden vor.
#'
#' @author Gerald Kaendler \email{gerald.kaendler@@forst.bwl.de}
#' @section Erstellungsdatum: 20.09.2013
#' @section Aktualisierungen: 01.12.2014 leere Menge korrigiert
#' @section Note: in \code{stratum} wird zunaechst die Gesamt-Menge, also die
#'  vollstaendige Tabelle \code{ecken} uebergeben, die dann entsprechend der in
#'  der Liste \code{auswahl} uebergebenen Attribute und den zulaessigen
#'  Werte-Mengen als Untermenge heraus gefiltert wird.
#' @section Refactored:
#' The whole thing Fri Aug 12 15:33:04 CEST 2016 by
#' \email{dominik.cullmann@@forst.bwl.de}.
#'  See \emph{use_classic}.
#' @param auswahl Liste, welche die Eckenmerkmale mit den Werten enthaelt,
#'  anhand derer die Auswahl erfolgt.
#' @param ecken Tabelle mit allen zur Selektion dienenden Eckenmerkmalen.
#' @param ecken_only Sollen nur TNr, ENr und die Auswahlmerkmale zurueckgegeben
#' werden? 
#' @param use_classic  Benutze den alten Algorithmus?
#' @export
#' @return Untermenge der Ecken, die der Auswahl entsprechen.
#' @examples
#' mbmb <- microbenchmark::microbenchmark
#' ecken <- get_data("ecken.3", package = "bundeswaldinventur")
#' auswahl <- list(Wa = c(3, 5), Begehbar = 1) 
#' gerald <- stratum.fun(auswahl, ecken, use_classic = TRUE)
#' dominik <- stratum.fun(auswahl, ecken, use_classic = FALSE)
#' RUnit::checkTrue(all.equal(gerald[order(gerald$TNr, dominik$ENr), TRUE],
#'                            dominik[order(dominik$TNr, dominik$ENr), TRUE],
#'                            check.attributes = FALSE))
#' print(mbmb(gerald = stratum.fun(auswahl, ecken, use_classic = TRUE),
#'            dominik = stratum.fun(auswahl, ecken, use_classic = FALSE),
#'            times = 100L
#'            ))
stratum.fun <- function(auswahl, ecken, ecken_only = TRUE, use_classic = TRUE) {
    # safe original names, convert to lowercase, restore on exit.
    names_auswahl <- names(auswahl)
    names(auswahl) <- tolower(names_auswahl)
    names_ecken <- names(ecken)
    names(ecken) <- tolower(names_ecken)
    if (isTRUE(use_classic)) {
        ecken <- as.data.frame(ecken)
        #Attribute für Auswahl
        attribute <- names(auswahl)
        k <- length(attribute)
        n <- length(ecken[,1])
        pos <- rep(0,k)
        #Position in der Tabelle <ecken> (Spalten-Nr.) und jeweilige Teilmenge
        #bestimmen
        stratum <- ecken
        for (i in 1:k)
        {
            #pos[i] <- grep(attribute[i],names(ecken),fixed=T)[1]
            #exaktes "matching"  kä/23.01.2015
            pos[i] <- which(names(ecken)==attribute[i])
            stratum <- subset(stratum,stratum[,pos[i]]%in%auswahl[[i]])
            if (is.factor(stratum[,pos[i]])) stratum[,pos[i]] <-
                as.numeric(stratum[,pos[i]])
        }
        stratum <- subset(stratum,select=c(1,2,pos))
        n.stratum <- length(stratum[,1])
        stratum[is.na(stratum)] <- 0
    } else {
        if (is.logical(auswahl)) {
            if (isTRUE(ecken_only)) {
                stratum <- ecken[auswahl,
                                 grep(("tnr|enr"), names(ecken),
                                      ignore.case = TRUE)]
            } else {
                stratum <- ecken[auswahl, TRUE]
                #stratum[is.na(stratum)] <- 0 # TODO: Why that?
            }
        } else {
            # Quote characters in list via paste() to preserve them while pasting.
            # Character vectors of length 1 go to shQuote(), others to paste() for
            # unkown reasons. Found out by try and error.
            idx <- which(unlist(lapply(auswahl, is.character)) &
                         unlist(lapply(auswahl, length)) == 1)
            auswahl[idx] <- shQuote(auswahl[idx])
            idx <- which(unlist(lapply(auswahl, is.character)))
            # now this is some magic I don't understand:
            auswahl[idx] <- paste0(auswahl[idx]) 
            columns <- paste0('ecken[["', names(auswahl), '"]]')
            condition <- paste(columns, auswahl, sep = " %in% ", collapse = " & ")
            if (isTRUE(ecken_only)) {
                return_columns <- deparse(c("tnr", "enr", names(auswahl)))
            } else {
                return_columns <- paste(deparse(names(ecken)), collapse = "")
            }
            text <- paste0("ecken[", condition, ", ", return_columns, "]")
            stratum <- eval(parse(text = text))
            # Convert factors like the original.
            idx <- which(sapply(stratum, class) == "factor")
            values <- as.numeric(unlist(stratum))
            dim(values) <- dim(stratum)
            colnames(values) <- names(stratum)
            stratum <- as.data.frame(values)
            stratum[is.na(stratum)] <- 0 # TODO: Why that?
        }
    }
    names(stratum) <- names_ecken[which(names(ecken) %in% names(stratum))]
    return(stratum)
}
