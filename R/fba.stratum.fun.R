#' Berechnet Verteilung der Dichtestufen
#'
#' Funktion berechnet Verteilung der Dichtestufen fuer Forstlich bedeutsame
#' Arten (FBA) entsprechend der Definition der BWI 3; die Funktion kann auch
#' fuer die entsprechende FBA-Tabelle der BWI 2 <fba.2> angewandt werden
#' (dort sind aber nur die Arten 11 bis 18 erfasst).
#'
#' @author Gerald Kaendler \email{gerald.kaendler@@forst.bwl.de}
#' @section Version: 01.05.2015
#' @section Note: Folgende FBA sind bei BWI 3 definiert:Adlerfarn, Brennessel,
#'  Riedgras, Honiggras, Reitgras, Heidekraut, Heidelbeere, Brombeere,
#'  Riesenbaerenklau, Riesenknoeterich, Druesiges Springkraut, Kleinbluetiges
#'  Springkraut, Kermesbeere; mit Code 11:23.
#' @section Hinweis: fehlende Angaben ("keine Angabe") werden als Nicht-
#'  Vorkommen interpretiert. \cr
#'  Zur Flaechenberechnung: Die Beobachtung des Vorkommens wird in visuell
#'  angesprochene Dichtestufen angegeben: 0: "nicht vorhanden", 1: "selten, bis
#'  10%", 2: "haeufig, > 10 bis 50%", 3:"flaechig, > 50%". Diese Dichten werden in
#'  Flaechenanteile umgerechnet: 0: 0; 1: 0.05; 2: 0.3; 3: 0.7; da jede
#'  Stichprobe mit dem Repraesentationsfaktor (RF) hochgerechnet wird, ergeben
#'  sich hieraus Flaechenschaetzungen in ha
#'  d.fl.ant <- data.frame(dichte=0:3,fl.ant=c(0,0.05,0.3,0.7))
#' @param fba Vektor mit den auszuwertenden FBA, wird NA oder "alle" uebergeben,
#'  werden alle ausgewertet.
#' @param fba.tab Die bei der BWI 3 erfolgte FBA-Dichte-Ansprachen je Traktecke.
#' @param auswahl Liste, welche die Eckenmerkmale mit den Werten enthaelt,
#'  anhand derer die Auswahl fuer das Stratum erfolgt.
#' @param ecken Ecken-Tabelle.
#' @param trakte Trakte-Tabelle.
#' @param A Flaeche des Inventurgebietes in ha.
#' @export
#' @return Liste mit folgenden Attributen: \strong{Stratum}, \strong{HBF}
#'  (Holzbodenflaeche), \strong{se.HBF} (Standardfehler Holzbodenflaeche),
#'  \strong{FBA}, \strong{Dichtestufen}, \strong{FBA.Dichtevertlg},
#'  \strong{FBA.Flaeche}. Wobei FBA.Dichtevertlg ein Array mit den
#'  Dichteanteilen der FBA ist und FBA.Flaeche ein Array ist, welches Wert und
#'  Standardfehler fuer Flaeche (ha) und Flaechenanteile enthaelt.
fba.stratum.fun <- function(fba, fba.tab, auswahl, ecken, trakte, A) {
  # Auswertungseinheit festlegen
  stratum <- stratum.fun(auswahl, ecken)
  # Kleinschreibung
  names(stratum) <- tolower(names(stratum))
  names(trakte) <- tolower(names(trakte))
  # Holzbodenfl\u00e4che des Stratums
  y <- stats::aggregate(rep(1, length(stratum[, 1])), by = list(stratum$tnr), sum)
  names(y) <- c("tnr", "y")
  # Teilmenge der Trakte im Auswertungsstratum
  y <- merge(trakte[TRUE, c("tnr", "m")],
    y,
    by = c("tnr"), all.x = T
  )
  y[is.na(y)] <- 0
  r.list <- r.variance.fun(y[, 2:3], length(trakte[, 1]))
  T.hbf <- r.list$R.xy * A
  se.T.hbf <- sqrt(r.list$V.R.xy) * A
  #-------------------
  nte.t <- stats::aggregate(rep(1, length(stratum[, 1])), by = list(stratum$tnr), sum)
  names(nte.t) <- c("tnr", "nte")
  #--------------------
  # FBA
  names(fba.tab) <- tolower(names(fba.tab))
  fba.tab <- merge(stratum[TRUE, c("tnr", "enr")],
    fba.tab[TRUE, c("tnr", "enr", "fba", "dichte")],
    by = c("tnr", "enr"), all.x = T
  )
  # fehlende Angaben werden als Nicht-Vorkommen interpretiert
  fba.tab$dichte[is.na(fba.tab$dichte)] <- 0
  # Falls aus Originaldaten Code -1 f\u00fcr fehlende Angabe vorkommt
  fba.tab$dichte[fba.tab$dichte == -1] <- 0

  fba.lab <- c(
    "Adlerfarn", "Brennessel", "Riedgras", "Honiggras", "Reitgras",
    "Heidekraut", "Heidelbeere", "Brombeere", "Riesenb\u00e4renklau",
    "Riesenkn\u00f6terich", "Dr\u00fcsiges Springkraut", "Kleinbl\u00fctiges Springkraut",
    "Kermesbeere"
  )
  fba.code <- c(11:23)

  if (is.na(fba[1]) | tolower(fba[1]) == "alle") {
    fba <- c(11:23)
  }

  # d.lab <- c("k.A.","fehlt","selten","h\u00e4ufig","fl\u00e4chig")
  # entsprechend den Codes -1, 0, 1, 2, 3
  d.lab <- c("nicht vorhanden", "selten, bis 10%", "h\u00e4ufig, > 10 bis 50%", "fl\u00e4chig, > 50%")
  # entsprechend den Codes 0, 1, 2, 3
  # entsprechende Fl\u00e4chenanteile f\u00fcr 0,1,2,3
  d.fl.ant <- data.frame(dichte = 0:3, fl.ant = c(0, 0.05, 0.3, 0.7))
  fba.tab <- merge(fba.tab, d.fl.ant, by = "dichte", all.x = T)
  fba.tab <- fba.tab[TRUE, c(2:4, 1, 5)] # TODO: Use named index.
  # Auf \u00fcberschie\u00dfende Fl\u00e4che pr\u00fcfen
  n.fba.te <- stats::aggregate(cbind(ifelse(fba.tab$dichte <= 0, 0, 1), fba.tab$fl.ant),
    by = list(fba.tab$tnr, fba.tab$enr), sum
  )
  names(n.fba.te) <- c("tnr", "enr", "n.fba", "sum.fl.ant")
  # \u00dcberschie\u00dfende Fl\u00e4che auf eins korrigieren
  fba.tab <- merge(fba.tab, n.fba.te, by = c("tnr", "enr"), all.x = T)
  # fba.tab$fl.ant.0 <- fba.tab$fl.ant
  fba.tab$fl.ant <- ifelse(fba.tab$sum.fl.ant > 1, fba.tab$fl.ant / fba.tab$sum.fl.ant, fba.tab$fl.ant)

  #---------------------
  # Ausgabe-Tabelle als Array
  # Dichte-Anteile
  # jeweils Wert und Fehler f\u00fcr 4 Dichte-Stufen nach FBA
  fba.d <- array(dim = c(2, 4, length(fba)))
  # Fl\u00e4chen und Fl\u00e4chenanteile
  # jeweils Wert und Fehler f\u00fcr FBA f\u00fcr Fl\u00e4che (ha) und Fl\u00e4chenanteil
  fba.fl <- array(dim = c(2, length(fba), 2))

  ii <- 0
  for (i in fba)
  {
    ii <- ii + 1
    fba.i <- merge(stratum,
      fba.tab[
        fba.tab[["fba"]] == i,
        c("tnr", "enr", "fba", "dichte", "fl.ant")
      ],
      by = c("tnr", "enr"),
      all.x = T
    )
    fba.i$dichte[is.na(fba.i$dichte)] <- 0
    fba.i$fl.ant[is.na(fba.i$fl.ant)] <- 0

    for (j in 0:3)
    {
      nte.d.j.t <- stats::aggregate(ifelse(fba.i$dichte == j, 1, 0), by = list(fba.i$tnr), sum)
      names(nte.d.j.t) <- c("tnr", "nd")
      nte.d.j.t <- merge(nte.t, nte.d.j.t, by = c("tnr"), all.x = T)
      nte.d.j.t[is.na(nte.d.j.t)] <- 0
      r.list <- r.variance.fun(nte.d.j.t[, 2:3], length(trakte[, 1]))
      fba.d[1, (j + 1), ii] <- round(r.list$R.xy, 4)
      fba.d[2, (j + 1), ii] <- round(sqrt(r.list$V.R.xy), 5)
    }
    # Fl\u00e4che sch\u00e4tzen als Total
    fl.i.t <- stats::aggregate(fba.i$fl.ant, by = list(fba.i$tnr), sum)
    names(fl.i.t) <- c("tnr", "fl")
    # daher Verkn\u00fcpfung mit <y>
    fl.i.t <- merge(y[, 1:2], fl.i.t, by = "tnr", all.x = T)
    fl.i.t[is.na(fl.i.t)] <- 0
    r.list <- r.variance.fun(fl.i.t[, 2:3], length(trakte[, 1]))
    fba.fl[1, ii, 1] <- round(r.list$R.xy * A, 1)
    fba.fl[2, ii, 1] <- round(sqrt(r.list$V.R.xy) * A, 2)

    # Fl\u00e4che sch\u00e4tzen als Anteil
    fl.i.t <- stats::aggregate(fba.i$fl.ant, by = list(fba.i$tnr), sum)
    names(fl.i.t) <- c("tnr", "fl")
    # Verkn\u00fcpfung mit <nte.t>
    fl.i.t <- merge(nte.t, fl.i.t, by = "tnr", all.x = T)
    fl.i.t[is.na(fl.i.t)] <- 0
    r.list <- r.variance.fun(fl.i.t[, 2:3], length(trakte[, 1]))
    fba.fl[1, ii, 2] <- round(r.list$R.xy, 4)
    fba.fl[2, ii, 2] <- round(sqrt(r.list$V.R.xy), 4)
  }

  return(list(
    Stratum = auswahl,
    HBF = T.hbf, se.HBF = se.T.hbf,
    FBA = fba.lab[match(fba, fba.code)],
    Dichtestufen = d.lab,
    FBA.Dichtevertlg = fba.d,
    "FBA.Fl\u00e4che" = fba.fl
  ))
}
