#' Aggregiert den ausgeschiedenen Vorrat
#'
#' Funktion wertet nach freien Baumarten-Gruppen und Alters- und Durchmesser-
#' Klassen im Stratum den ausgeschiedenen Vorrat aus (Derbholz mR, Erntevolumen
#' oR, oberird. Biomasse oiB). Im Unterschied zu
#' \code{VB.A.bagrupp.akl.dkl.stratum.fun.3} wird hier als 4. Groesse die
#' ausgeschiedene Stammzahl berechnet (in Version .3 das Erntevolumen im
#' Hauptbestand).
#'
#' @author Gerald Kaendler \email{gerald.kaendler@@forst.bwl.de}
#' @section Erstellungsdatum: 14.03.2014
#' @section Achtung: Voraussetzung ist, dass die Tabellen \code{baeume.23}
#'  (Baeume_B-Tabelle der Vorinventur (fuer BWI 3 die Tabelle
#'  "BWI_23_Baeume_B"), folgende Attribute muessen mind. enthalten sein: TNr,
#'  ENr, BA, Pk, Alt2, BHD2, VolV2, oiB2, NHa1, StFl1. Man kann auch die
#'  selektierten Attribute mit Namen ohne Kennziffer uebergeben, wenn bereits
#'  eine eindeutige Auswahl der Attribute in <baeume> uebergeben wurde.),
#'  \code{baeume.3}, (Baeume der Folgeinventur) \code{ecken.2}, \code{ecken.3},
#'  \code{trakte.3} sowie \code{bacode} eingelesen sind! \cr
#'  Das Baumattribut <Pk> muss enthalten sein!
#' @section Hinweis: Version mit freier Baumartengruppierung sowie jaehrlicher
#'  flaechenbezogenen Nutzung analog flaechenbezogenem jaehrlichen Zuwachs sowie
#'  Option einer Differenzierung nach Nutzungsart. \cr
#'  Ratio-Schaetzer-Varianz ueber \code{r.variance.fun} (Matrizen-Notation)
#'  berechnet! \cr
#'  Version fuer Periode BWI 2 zu 3! \cr
#'  Auswertung erfolgt auf dem gemeinsamen Netz im Unterschied zu
#'  der historischen Version VB.A.BAGR.akl.dkl.stratum.fun, welche das Nutzungsgeschehen
#'  auf der bei der BWI 2 erfassten Flaeche abdeckte! \cr
#'  Fuer die Berechnung der flaechenbezogenen Nutzungen muss die mittlere
#'  Baumartenflaeche der Periode berechnet werden, d.h. es werden auch die
#'  Standflaechen der Folgeaufnahme benoetigt! \cr
#'  Dies wird von der Funktion \code{\link{mbaf.bagr.alt.bhd.pm.fun}}
#'  uebernommen! \cr
#'  Um Konflikte mit unterschiedlicher Gross- und Kleinschreibung bei den
#'  Attributnamen zu vermeiden, werden innerhalb dieser Funktion alle
#'  Attributnamen auf Kleinschreibung umgestellt.
#' @section TODO: Verallgemeinerung bzw. Variante fuer BWI 1 zu 2 !!!!
#' @param BA.grupp Liste mit Baumarten-Zusammenfassungen zu Baumgruppen mit
#'  Bezeichner der Baumarten-Gruppen ("lab") z.B. list(bagr.lab = c("FiTa",
#'  "DglKiLae", "Bu", "Ei", "BLb", "WLb"), ba.grupp =list(c(10:19,30:39,90:99),
#'  c(20:29,40,50,51), c(100), c(110,111), c(112:199),c(200:299))).
#' @param A.klass Liste mit den Klassifizierungsparametern fuers Alter: z.B.
#'  list(A.ob=160, A.b=20).
#' @param D.klass Liste mit den Klassifizierungsparametern fuer Durchmesser z.B.
#'  list(D.unt=0, D.ob=70, D.b=10, Ndh=T), Ndh (Nicht-Derbholz) = T bedeutet,
#'  dass zusaetzlich Nicht-Dh (unter 7 cm) ausgewiesen wird, sonst gilt
#'  \code{D.unt} als unterste Schwelle.
#' @param auswahl Liste, welche die Eckenmerkmale mit den Werten
#'  enthaelt, anhand derer die Auswahl fuer das Stratum erfolgt. Bsp.:
#'  list(Wa=c(3,5), Begehbar=1).
#' @param N.art Differenzierung nach Nutzungsart. TRUE: Trennung nach "geerntet"
#'  = Pk 2,3,9 und "ungenutzt" = Pk 4, 5; FALSE: keine Trennung!
#' @param A Gesamtflaeche des Inventurgebiets in ha zum jeweiligen
#'  Inventurzeitpunkt (sollte eigentlich konstant sein).
#' @inheritParams fvbn.kreis.fun.1
#' @param baeume.23 Tabelle mit Baumdaten aus BWI 2.
#' @export
#' @return Liste mit folgenden Komponenten: \strong{Log} (Liste mit Datum und
#'  genutzter Baumversion), \strong{Stratum} (\code{auswahl}), \strong{nTE}
#'  (Anzahl Ecken im Stratum), \strong{HBF} (Holzbodenflaeche in ha),
#'  \strong{se.HBF} (Standardfehler der HBF), \strong{Attribute} (Vektor mir
#'  berechneten Attributen), \strong{Groessen} (Vektor mit berechneten Groessen
#'  fuer Attribute (Wert, Standardfehler)), \strong{Nutzungsart} (2 Kategorien:
#'  geernet oder ungenutzt, wenn diese nicht definiert sind, wird "insgesamt"
#'  ausgegeben), \strong{BAGR} (Labels fuer Baumartengruppen aus
#'  \code{ba.grupp}), \strong{AKL} (Labels der Altersklassen), \strong{DKL}
#'  (Labels der Durchmesserklassen), \strong{T.VBN.A.NArt.Bagr.Akl.Dkl} (Array
#'  mit berechneten Groessen (Wert und Standardfehler) fuer das ausgeschiedene
#'  Kollektiv), \strong{BAF.bagr.akl.dkl} (Array mit Werten und Standardfehlern
#'  zu Baumartenflaechen nach Baumartengruppen, Alterklassen und
#'  Durchmesserklassen), \strong{mPL.NArt.Bagr.Akl.Dkl} (mittlere kalkulierte
#'  Periodenlaenge mit Standardfehler), \strong{mPL.Stratum} (mittlere
#'  Periodenlaenge als gewogenes Mittel), \strong{se.mPL.Stratum}
#'  (Standardfehler der mittleren Periodenlaenge (mPl.Stratum)),
#'  \strong{nT.NArt.Bagr.Akl.Dkl} (Anzahl Trakte je Nutzungsart,
#'  Baumartengruppen, Altersklassen und Durchmesserklassen).
#' @examples
#' bagrupp <- list(
#'   bagr.lab = c("FiTa", "DglKiLae", "Bu", "Ei", "BLb", "WLb"),
#'   ba.grupp = list(
#'     c(10:19, 30:39, 90:99), c(20:29, 40, 50, 51),
#'     c(100), c(110, 111), c(112:199), c(200:299)
#'   )
#' )
#' aklass <- list(A.ob = 160, A.b = 20)
#' dklass <- list(D.unt = 0, D.ob = 70, D.b = 10, Ndh = TRUE)
#' auswahl <- list(Wa = c(3, 5), Begehbar = 1)
#' A <- bundeswaldinventur::get_design("A", 3)
#' r <- VB.A.bagrupp.akl.dkl.stratum.fun.2(
#'   BA.grupp = bagrupp,
#'   A.klass = aklass,
#'   D.klass = dklass,
#'   auswahl = auswahl,
#'   N.art = TRUE,
#'   A = A,
#'   baeume.3 = get_data("baeume.3"),
#'   trakte.3 = get_data("trakte.3"),
#'   ecken.3 = get_data("ecken.3"),
#'   trakte.2 = get_data("trakte.2"),
#'   ecken.2 = get_data("ecken.2"),
#'   baeume.23 = get_data("baeume.23")
#' )
VB.A.bagrupp.akl.dkl.stratum.fun.2 <-
  function(BA.grupp, A.klass, D.klass, auswahl, N.art, A,
           baeume.3 = get_data("baeume.3"),
           trakte.3 = get_data("trakte.3"),
           ecken.3 = get_data("ecken.3"),
           trakte.2 = get_data("trakte.2"),
           ecken.2 = get_data("ecken.2"),
           baeume.23 = get_data("baeume.23")) {
    # Bei Fl\u00e4chenbezug Reduktion auf gemeinsames Netz!!
    # k\u00e4/28.02.2014
    #---
    # k\u00e4/24.08.2014: Bei Stratifikation anhand von Eckenmerkmalen gilt die
    # aktuelle Inventur (BWI 3), z.B. die Eigentumsklassenzuordnung
    # alte Fassung:
    # auswahl$Begehbar=1; auswahl$Wa=c(1:3)
    # ecken.2.s <- stratum.fun(auswahl,ecken.2)
    # neu:
    ecken.2.s <- stratum.fun(list(Wa = c(1:3), Begehbar = 1), ecken.2)
    #---
    auswahl$Wa <- c(3, 5)
    ecken.3.s <- stratum.fun(auswahl, ecken.3)
    # gemeinsames Netz Land BW BWI 2 und 3 auf begehbarem Holzboden
    ecken.23.hb <- merge(ecken.3.s[TRUE, c("TNr", "ENr")],
      ecken.2.s[TRUE, c("TNr", "ENr")],
      by = c("TNr", "ENr")
    )
    ecken.23.hb <- merge(ecken.23.hb, ecken.3[TRUE, c("TNr", "ENr", "PL", "PLkal")],
      by = c("TNr", "ENr")
    )
    stratum <- ecken.23.hb
    trakte <- trakte.3
    t.pos <- length(trakte) # Anzahl Spalten in <trakte> wird ben\u00f6tigt, um
    # Attribut-Positionen zu bestimmen
    #--------------------
    # inv <- 1
    # stratum <- stratum.fun(auswahl,ecken)
    # Kleinschreibung
    names(stratum) <- tolower(names(stratum))
    names(trakte) <- tolower(names(trakte))
    n.te.s <- length(stratum[, 1])
    # <y> steht hier f\u00fcr die Anzahl der Traktecken auf begehbarem HB im Stratum
    y <- stats::aggregate(rep(1, length(stratum[, 1])), by = list(stratum$tnr), sum)
    names(y) <- c("tnr", "y")
    # Anzahl der Trakte im Stratum
    n.t.s <- length(y[, 1])
    # Anf\u00fcgen der Anzahl Traktecken (Wald und Nicht-Wald)
    y <- merge(y, trakte[TRUE, c("tnr", "m")], by = c("tnr"))
    # this was:
    #  y <- merge(y,subset(trakte,select=c(tnr,m),by=c(tnr)))
    # where the ellipsis of subset() swallows up the misplaces by argument (meant
    # for merge())!

    # Alle Traktecken im Inventurgebiet
    x <- trakte$m
    # n Trakte im Inventurgebiet ist konstant
    nT <- length(trakte[, 1])
    #----------------
    # HBFl. [ha]
    T.hbf <- sum(y$y) / sum(x) * A
    var.T.hbf <- nT / (nT - 1) * T.hbf^2 * (sum(y$y^2) / sum(y$y)^2 + sum(x^2) / sum(x)^2
      - 2 * sum(y$y * y$m) / sum(y$y) / sum(x))
    se.T.hbf <- var.T.hbf^0.5 # Standardfehler
    # Hier k\u00f6nnte als Alternative die Funktion <r.variance.fun> benutzt werden
    # Hierzu m\u00fcsste eine alle Trakte umfassende Matrix <xy> mit <m> und <y>
    # \u00fcbergeben werden
    #----------------
    # k\u00e4/28.02.2014:
    baeume <- baeume.23 # muss eingelesen sein!
    # Kleinschreibung aller Atttributnahmen in <baeume>
    names(baeume) <- tolower(names(baeume))

    # HINWEIS: beim ausgeschiedenen Vorrat wird der zur Periodenmitte
    # fortgeschriebene Vorrat verwendet! volv2,vole2,oib2
    baeume.s <- merge(
      baeume[TRUE, c("tnr", "enr", "stp", "bnr", "ba", "pk", "alt1", "alt2", "bhd1", "bhd2", "volv2", "vole2", "oib2", "nha1", "stfl1")],
      stratum[TRUE, c("tnr", "enr", "pl", "plkal")],
      by = c("tnr", "enr")
    )

    # BA-Gruppe dazu spielen
    # Baumartengruppen-Zuordnungstabelle f\u00fcr BWI-BA-Code erzeugen
    # (Tab. <bacode> muss geladen sein)
    bagr.tab <- ba.klass.lab.tab.fun(BA.grupp)
    n.bagr <- length(BA.grupp[[1]])
    # BAGR-Liste
    bagr.list <- BA.grupp[[1]]
    n.bagr <- length(bagr.list)

    baeume.s <- merge(baeume.s, bagr.tab[TRUE, c("ICode", "bagr")],
      by.x = "ba", by.y = "ICode", all.x = T
    )
    names(baeume.s) <- tolower(names(baeume.s))

    # Folgeinventur: BA-Gruppen hinzuf\u00fcgen
    baeume.3.s <- merge(baeume.3[TRUE, c("TNr", "ENr", "STP", "BNr", "Pk", "BA", "Alt1", "Alt2", "BHD1", "BHD2", "StFl2")],
      bagr.tab[TRUE, c("ICode", "bagr")],
      by.x = "BA", by.y = "ICode", all.x = T
    )
    names(baeume.3.s) <- tolower(names(baeume.3.s))
    baeume.3.s <- merge(baeume.3.s, stratum[TRUE, c("tnr", "enr", "pl", "plkal")],
      by = c("tnr", "enr")
    )

    #--------------------------------------------------
    # Mittlere Baumartenfl\u00e4chen nach BAGr, AKl und DKl zur Periodenmitte nach Trakt
    mbaf.bagr.akl.dkl.tnr <- mbaf.bagr.alt.bhd.pm.fun(
      baeume.s, baeume.3.s, A.klass, D.klass
    )

    #-------------
    # Klassifizierung des Ausgeschiedenen Vorrats durchf\u00fchren
    # Alter
    A.max <- 999
    # Hinweis: A-Klassifizierung nach fortgeschriebenem Alter: alt2!!!
    baeume.s$akl <- cut(baeume.s$alt2,
      breaks = c(seq(0, A.klass[[1]], A.klass[[2]]), A.max), right = T
    )
    akl.lab <- unique(baeume.s$akl)
    akl.lab <- as.character(akl.lab[order(akl.lab)])
    A.k <- length(akl.lab) - 1 # wegen NA (Alter 0 ausgeschlossen!)
    if (A.k == 0) A.k <- 1
    # Durchmesser
    D.max <- 999
    if (D.klass[["Ndh"]] & D.klass[[1]] < 7) {
      brks <- c(0, 7, seq(D.klass[[1]] + D.klass[[3]], D.klass[[2]], D.klass[[3]]), D.max)
    } else {
      brks <- c(seq(D.klass[[1]], D.klass[[2]], D.klass[[3]]), D.max)
    }
    # Hinweis: D-Klassifizierung nach fortgeschriebenem BHD: bhd2!!!
    baeume.s$dkl <- cut(baeume.s$bhd2, breaks = brks, right = F)
    dkl.lab <- unique(baeume.s$dkl)
    dkl.lab <- as.character(dkl.lab[order(dkl.lab)])
    D.k <- length(dkl.lab[!is.na(dkl.lab)])

    # Array f\u00fcr mittlere BAF zur PM nach BAGr, AKl, DKl
    BAF.bagr.akl.dkl <- array(dim = c(2, n.bagr, A.k, D.k))

    if (N.art) {
      pk.list <- list(c(2, 3, 9), c(4, 5))
      n.nart <- 2
    } else {
      pk.list <- list(c(2:5, 9))
      n.nart <- 1
    }
    # Array f\u00fcr Ergebnisse (Totals und SE jeweils nach Nutzungsart, BAGr, AKl, DKl)
    # Nutzungsart: aus <pk>: 2 = selektiv genutzt,  3 = fl\u00e4chig genutzt
    #                       4 = am Ort verblieben, 5 = abgestorben  (stehend)
    #                       9 = unauffindbar (wird der Kategorie geerntet
    #                           zugewiesen)
    #                       2,3,9 definieren den geernteten ausgeschiedenen Vorrat
    #                       4, 5 definieren den ungenutzten (tw. aus nat\u00fcrl. Mort.
    #                       stammenden) ungenutzten ausgeschiedenen Vorrat)
    # Aus diesen Kennzahlen werden die 2 Kategorien der Nutzungsart (NArt):
    # geerntet bzw. ungenutzt festgelegt.
    # Es gibt 4 Zielgr\u00f6\u00dfen <Y>:  V [m^3Dh mR], V Eor (Erntevolumen o. R.) [m^3E oR],
    # B (oberird. Biomasse) [t], N (Anzahl), f\u00fcr die jeweils der Gesamtwert der
    # Periode ("Total") und der j\u00e4hrliche Wert berechnet wird, sowie der
    # Stichprobenfehler (SE), und zwar jeweils f\u00fcr die 2 Kategorien "geerntet" /
    # "ungenutzt" sowie 9 Baumartengruppen, A.k Alters- und D.k Durchmesserklassen
    # 1. Index: 1- 4: Perioden-Total (v, v.eor, b, n);
    #          4- 8: m. j\u00e4hrlicher Wert (j.v, j.v.eor, j.b, j.n);
    #          9-12: m. j\u00e4hrlicher Wert je ha     (k\u00e4/28.02.2014)
    # 2. Index: 1: Wert; 2: SE;
    # 3. Index: Nutzungsart (wenn <N.art> == TRUE (1: geerntet, 2: ungenutzt)
    # 4. Index: BAGr; 5. Index: A-Klasse; 6. Index: D-Klasse
    Y.na.bagr.akl.dkl <- array(dim = c(12, 2, n.nart, n.bagr, A.k, D.k))
    # mittlere kal. Periodenl\u00e4nge mit Standard-Fehler
    mPL.na.bagr.akl.dkl <- array(dim = c(2, 2, n.bagr, A.k, D.k))
    # Anzahl Trakte (PSU) je NArt, BAGR, Akl, Dkl
    nT.na.bagr.akl.dkl <- array(dim = c(n.nart, n.bagr, A.k, D.k))
    # Mittlere Straten-PL mit SE
    ne.T <- stats::aggregate(rep(1, length(stratum[, 1])), by = list(stratum$tnr), sum)
    names(ne.T) <- c("tnr", "n.te")
    # "Periodensumme" je Trakt (mit n Ecken gewogen)
    # Bei Nutzung wird kalendarische Periodenl\u00e4nge <plkal> verwendet!
    y.pl <- stats::aggregate(stratum$plkal, by = list(stratum$tnr), mean, na.rm = T)$x * ne.T$n.te
    # mittl. PL als gewogenes Mittel
    mpl.stratum <- sum(y.pl) / sum(ne.T$n.te)
    # Berechnung des Standardfehlers
    se.mpl.stratum <- mpl.stratum *
      sqrt(nT / (nT - 1) * (sum(y.pl^2) / sum(y.pl)^2 + sum(ne.T$n.te^2) / sum(ne.T$n.te)^2
        - 2 * sum(y.pl * ne.T$n.te) / sum(y.pl) / sum(ne.T$n.te)))

    #----------------


    for (i in 1:n.bagr) # Baumartengruppen
    {
      for (j in 1:A.k) # Altersklassen
      {
        for (k in 1:D.k) # Durchmesserklassen
        {
          # Baumartenfl\u00e4che zur Periodenmitte aggregieren
          baf.ba <- mbaf.bagr.akl.dkl.tnr[
            mbaf.bagr.akl.dkl.tnr[["bagr"]] == bagr.list[i] & mbaf.bagr.akl.dkl.tnr[["akl.pm"]] == akl.lab[j] & mbaf.bagr.akl.dkl.tnr[["dkl.pm"]] == dkl.lab[k],
            c("tnr", "mbaf")
          ]
          # Mit allen Trakten im Inventurgebiet vereinen
          xy.baf <- merge(trakte, baf.ba, by = c("tnr"), all.x = T)
          # NA eliminieren!
          xy.baf$mbaf[is.na(xy.baf$mbaf)] <- 0
          # xy.baf$mbaf <- xy.baf$mbaf
          # xy. <- cbind(xy$m,xy$mbaf/10000)
          # Total der Baumartenfl\u00e4che
          R.list <- r.variance.fun(cbind(xy.baf$m, xy.baf$mbaf), nT)
          BAF.bagr.akl.dkl[1, i, j, k] <- R.list$R.xy * A
          BAF.bagr.akl.dkl[2, i, j, k] <- sqrt(R.list$V.R.xy) * A

          for (i.n in 1:n.nart) # Nutzungsart (geerntet, ungenutzt), wenn gesetzt!
          {
            baeume.ba <- baeume.s[
              baeume.s[["pk"]] %in% pk.list[[i.n]] & baeume.s[["bagr"]] == bagr.list[i] & baeume.s[["akl"]] == akl.lab[j] & baeume.s[["dkl"]] == dkl.lab[k],
              c("tnr", "enr", "pk", "volv2", "vole2", "oib2", "nha1", "plkal")
            ]
            if (length(baeume.ba[, 1]) == 0) {
              Y.na.bagr.akl.dkl[, 1, i.n, i, j, k] <- rep(0, 12) # Zielgr\u00f6\u00dfe
              Y.na.bagr.akl.dkl[, 2, i.n, i, j, k] <- rep(0, 12) # Stichprobenfehler (SE)

              mPL.na.bagr.akl.dkl[1:2, i.n, i, j, k] <- rep(0, 2)
              nT.na.bagr.akl.dkl[i.n, i, j, k] <- 0 # n PSU (Trakte)
            } else {
              # Nach Trakt aggregieren
              # fortgeschrieben: volv2, vole2, oib2!!!!
              # Ausgeschiedener Derbholz-Vorrat [m^3 mR] als "v"
              xy <- stats::aggregate(baeume.ba$volv2 * baeume.ba$nha1,
                by = list(baeume.ba$tnr),
                sum
              )
              names(xy) <- c("tnr", "v")
              # Ausgeschiedener Vorrat Erntevolumen [m^3 oR] als "v.eor"
              xy <- cbind(
                xy,
                stats::aggregate(baeume.ba$vole2 * baeume.ba$nha1,
                  by = list(baeume.ba$tnr), sum
                )$x
              )
              names(xy)[3] <- "v.eor"
              # Ausgeschiedener Vorrat in oberird. Biomasse [t] als "b"
              xy <- cbind(xy, stats::aggregate(baeume.ba$oib2 * baeume.ba$nha1,
                by = list(baeume.ba$tnr), sum
              )$x / 1000)
              names(xy)[4] <- "b"
              # Anzahl B\u00e4ume als "n"
              xy <- cbind(xy, stats::aggregate(baeume.ba$nha1,
                by = list(baeume.ba$tnr),
                sum
              )$x)
              names(xy)[5] <- "n"
              # Mittlere kal. Periodenl\u00e4nge je Trakt
              mpl <- stats::aggregate(baeume.ba$plkal, by = list(baeume.ba$tnr), mean)$x

              # J\u00e4hrlicher ausgeschiedener Derbholzvorrat
              xy$j.v <- xy$v / mpl
              # J\u00e4hrlicher ausgeschiedener Erntevorrat
              xy$j.v.eor <- xy$v.eor / mpl
              # J\u00e4hrlicher ausgeschiedener oi. Biomassevorrat
              xy$j.b <- xy$b / mpl
              # J\u00e4hrliche ausgeschiedene Stammzahl
              xy$j.n <- xy$n / mpl

              # Anzahl Traktecken je Trakt (Wald- und Nichtwald) und <mbaf>
              # hinzuf\u00fcgen   Hinweis: <xy.baf> enth\u00e4lt jetzt auch <m_bhb>!
              # xy <- merge(xy,xy.baf,by=c("tnr"))
              xy <- merge(xy.baf, xy, by = c("tnr"), all.x = T)
              # for (ii in 7:14) {xy[is.na(xy[,ii]),ii] <- 0}
              xy[is.na(xy)] <- 0

              # Anzahl Trakte (i.S. von PSU) im Teilkollektiv i.n,i,j,k
              nT.na.bagr.akl.dkl[i.n, i, j, k] <- length(xy[, 1])

              # mittlere kal. Periodenl\u00e4nge mit Standard-Fehler
              # Anzahl Ecken je Trakt bestimmen
              nb.TE <- stats::aggregate(rep(1, length(baeume.ba[, 1])),
                by = list(baeume.ba$tnr, baeume.ba$enr), sum
              )
              ne.T <- stats::aggregate(rep(1, length(nb.TE[, 1])),
                by = list(nb.TE$Group.1),
                sum
              )
              names(ne.T) <- c("tnr", "n.te")
              # "Periodensumme" je Trakt (mit n Ecken gewogen)
              y.pl <- mpl * ne.T$n.te
              R.list <- r.variance.fun(cbind(ne.T$n.te, y.pl), nT)
              # mittl. PL als gewogenes Mittel
              mPL.na.bagr.akl.dkl[1, i.n, i, j, k] <- R.list$R.xy
              # alte Fassung
              # mPL.na.bagr.akl.dkl[1,i.n,i,j,k] <- sum(y.pl)/sum(ne.T$n.te)
              # Berechnung des Standardfehlers
              mPL.na.bagr.akl.dkl[2, i.n, i, j, k] <- sqrt(R.list$V.R.xy)

              for (l in 1:8) # 4 Totale, 4 J\u00e4hrliche Totale
              {
                # Zielgr\u00f6\u00dfen Y ausgeschiedenes Kollektiv{V,V.EoR,B,N)
                # Perioden-Total, j\u00e4hrl. Total, j\u00e4hrl. Ha-Wert (k\u00e4/01-03-2014)
                R.list <- r.variance.fun(cbind(xy$m, xy[, (l + t.pos + 1)]), nT)
                Y.na.bagr.akl.dkl[l, 1, i.n, i, j, k] <- R.list$R.xy * A
                # Y.na.bagr.akl.dkl[l,1,i.n,i,j,k] <- sum(xy[,(1+l)])/sum(x)*A
                # Zugeh\u00f6riger Stichprobenfehler
                Y.na.bagr.akl.dkl[l, 2, i.n, i, j, k] <- sqrt(R.list$V.R.xy) * A
              } # End for l (Zielgr\u00f6\u00dfen)
              # Offset f\u00fcr Spalten-Position der 4 j\u00e4hrliche Ha-Werte
              off <- length(xy) - 4
              # Fl\u00e4chenbezogene Zielgr\u00f6\u00dfen:
              for (l in 1:4) # 4 j\u00e4hrliche Ha-Werte
              {
                # Zielgr\u00f6\u00dfen Y ausgeschiedenes Kollektiv{V,V.EoR,B,N)
                # Perioden-Total, j\u00e4hrl. Total, j\u00e4hrl. Ha-Wert (k\u00e4/01-03-2014)
                R.list <- r.variance.fun(cbind(xy$mbaf, xy[, (l + off)]), nT)
                Y.na.bagr.akl.dkl[(l + 8), 1, i.n, i, j, k] <- R.list$R.xy
                # Zugeh\u00f6riger Stichprobenfehler
                Y.na.bagr.akl.dkl[(l + 8), 2, i.n, i, j, k] <- sqrt(R.list$V.R.xy)
              } #
            } # End if ... else
          } # End for i.n (Nutzungsart: geerntet, ungenutzt)
        } # End for k (D-Klassen)
      } # End for j (A-Klassen)
    } # End for i (BAGR)

    #-----------------------
    # AKL-Labels
    akl.lab <- akl.lab.fun(A.klass, A.k)

    # DKL-Labels
    dkl.lab <- dkl.lab.fun(D.klass, D.k) # k\u00e4/16.07.14

    # Tabelle f\u00fcr BA-Gruppen
    # Dokumentation der Grunddaten und Auswertungsdatum der HR
    a <- regexpr("/", baeume$bemerk[baeume$stp == 0][1], fixed = T)
    b <- nchar(as.character(baeume$bemerk[baeume$stp == 0][1]))
    version.baeume.b <- substr(as.character(baeume$bemerk[baeume$stp == 0][1]), a, b)
    Log <- list(
      Datum = Sys.time(),
      Version.baeume.b = substr(as.character(baeume$bemerk[baeume$stp == 0][1]), a, b)
    )

    return(list(
      Log = Log, Stratum = auswahl, nTE = n.te.s, HBF = T.hbf, se.HBF = se.T.hbf,
      Attribute = c(
        "V_DhmR", "V_EoR", "oiB", "N_Dh",
        "V_DhmR/J", "V_EoR/J", "oiB/J", "N_Dh/J",
        "V_DhmR/ha/J", "V_EoR/ha/J", "oiB/ha/J", "N_Dh/ha/J"
      ),
      "Gr\u00f6\u00dfen" = c("Wert", "Standardfehler"),
      Nutzungsart = n.nart,
      BAGR = bagr.list, AKL = akl.lab[1:A.k], DKL = dkl.lab,
      T.VBN.A.NArt.Bagr.Akl.Dkl = Y.na.bagr.akl.dkl,
      BAF.bagr.akl.dkl = BAF.bagr.akl.dkl,
      mPL.NArt.Bagr.Akl.Dkl = mPL.na.bagr.akl.dkl,
      mPL.Stratum = mpl.stratum, SE.mPL.Stratum = se.mpl.stratum,
      nT.NArt.Bagr.Akl.Dkl = nT.na.bagr.akl.dkl
    ))
  } # End <VB.A.bagrupp.akl.dkl.stratum.fun.2>
