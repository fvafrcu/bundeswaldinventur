#' Load Data from \pkg{bwibw}
#'
#' Get data if we want to use example data provided with this package or
#' \pkg{bwibw}.
#'
#' Allows to globally en/dis-abling the reading of example
#' data into function arguments, Which we need because Gerald used global
#' data.frames in his functions.
#'
#' @param name a string of length one giving the name of the data.frame.
#' @param package The package to load data from.
#' @param lowercase_names Convert all column names to lowercase?
#' @return the data requested.
#' @export
#' @examples
#' if ("bwibw" %in% rownames(installed.packages())) {
#'    data(package = "bwibw")
#'    get_data("bacode", package = "bwibw")
#' } else {
#'    get_data("bacode")
#' }
get_data <- function(name, 
                     package = get_options("data_source", 
                                           name = "bundeswaldinventur"),
                     lowercase_names = get_options("lowercase_names", 
                                           name = "bundeswaldinventur")
                     ) {

  checkmate::qassert(name, "S1")
  checkmate::assert(checkmate::checkNull(package),
                    checkmate::checkString(package))
  if (is.null(package)) package <- "bundeswaldinventur"
  utils::data(list = name, package = package, envir = environment())
  if (isTRUE(lowercase_names)) {
      tmp <- get(name)
      names(tmp) <- tolower(names(tmp))
      assign(name, tmp)
      rm(tmp)
  }
  return(get(name))
}
