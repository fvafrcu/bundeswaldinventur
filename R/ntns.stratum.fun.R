#' Berechnet Flaechen und Flaechenanteile der Naturnaehestufen
#'
#' Funktion berechnet fuer das in \code{auswahl} definierte Stratum die Flaechen
#' und Flaechenanteile (\%) der 5 Naturnaehestufen (ntns) fuer BWI 2 oder BWI 3
#'
#' @author Gerald Kaendler \email{gerald.kaendler@@forst.bwl.de}
#' @section Erstellungsdatum: 19.08.2014
#' @param ntns.te alle Traktecken die Naturnaehestufen in allen Varianten
#'  (Schichten, Gesamtwert fuer Hauptbestockung) fuer BWI 2 und 3 sowie fuerr BWI
#'  2 in den 2 Vaianten natWG BWI 2 und natWG BWI 3.
#' @param ecken Stichprobenmerkmale der jeweiligen BWI (2 oder 3).
#' @param trakte Traktmermale (m, m_HB, m_bHB, m_Wa).
#' @param A Gesamtflaeche des Inventurgebietes in ha.
#' @param auswahl Liste, welche die Eckenmerkmale mit den Werten enthaelt,
#'  anhand derer die Auswahl fuer das Stratum erfolgt. Bsp.: list(Wa=c(3,5),
#'  Begehbar=1).
#' @param bwi Angabe welche BWI (2 oder 3).
#' @param natwg Differenzierung bei Auswertung fuer BWI 2: 2: nat. WG der 3
#'  damaligen BWI 2 oder 3: bei BWI 3 aktualisierte nat. WG, die rueckwirkend
#'  auf BWI 2 angewandt wird.
#' @export
#' @return Liste mit folgenden Komponenten: \strong{Datum}, \strong{HBF}
#'  (Holzbodenflaeche in ha), \strong{se.HBF} (Standardfehler der
#'  Holzbodenflaeche), \strong{NTNS} (Vektor mit den verschiedenen ntns),
#'  \strong{NTNS.Flaeche.Anteil} (absoluter und relativer Flaechenanteil vom
#'  Inventurgebiet (?) und jeweiliger Standardfehler).
ntns.stratum.fun <- function(ntns.te, ecken, trakte, A, auswahl, bwi, natwg) {
  stratum <- stratum.fun(auswahl, ecken)
  if (bwi == 3) {
    stratum <- merge(stratum, ntns.te[TRUE, c("Tnr", "Enr", "NTNS_F_BaWue_BWI3")],
      by.x = c("TNr", "ENr"), by.y = c("Tnr", "Enr"), all.x = T
    )
    NTNS <- "BWI 3"
  } else { # BWI 2: Fallunterscheidung nach verwendeten nat. WG
    if (natwg == 3) {
      stratum <- merge(stratum,
        ntns.te[
          TRUE,
          c("Tnr", "Enr", "NTNS_F_BaWue_BWI2_natwgBWI3")
        ],
        by.x = c("TNr", "ENr"), by.y = c("Tnr", "Enr"), all.x = T
      )
      NTNS <- "BWI 2 mit nat. WG der BWI 3"
    } else { # Nat. WG der BWI 2
      stratum <- merge(stratum,
        ntns.te[TRUE, c("Tnr", "Enr", "NTNS_F_BaWue_BWI2")],
        by.x = c("TNr", "ENr"), by.y = c("Tnr", "Enr"), all.x = T
      )
      NTNS <- "BWI 2 mit nat. WG der BWI 2"
    }
  }
  names(stratum)[length(stratum)] <- "ntns"
  # Anzahl TE nach Trakt und NTNS aggregieren
  xy.ntns.t <- stats::aggregate(rep(1, length(stratum[, 1])),
    by = list(stratum$TNr, stratum$ntns), sum
  )
  names(xy.ntns.t) <- c("TNr", "ntns", "n.ntns")
  # Anzahl TE nach Trakt aggregieren
  xy.t <- stats::aggregate(rep(1, length(stratum[, 1])),
    by = list(stratum$TNr), sum
  )
  names(xy.t) <- c("TNr", "n")

  # Array definieren
  # 1. Dim (2): 1: Fl\u00e4che ha HB; 2: Prozent-Anteil;
  # 2. Dim (2): 1: Wert, 2: Standardfehler
  # 3. Dim (6): 1-5 Naturn\u00e4hestufen + "keine Angabe"

  ntns.f.ant <- array(dim = c(2, 2, 6))

  for (i in 1:6) {
    if (i < 6) {
      xy.ntns.i.t <- xy.ntns.t[xy.ntns.t[["ntns"]] == i, c("TNr", "n.ntns")]
    } else { # OHNE NTNS-Angabe
      xy.ntns.i.t <- xy.ntns.t[
        !xy.ntns.t[["ntns"]] %in% c(1:5),
        c("TNr", "n.ntns")
      ]
    }
    names(xy.ntns.i.t)[2] <- "n.ntns"
    xy.i.t <- merge(trakte[TRUE, c("TNr", "m")],
      xy.ntns.i.t,
      by = "TNr", all.x = T
    )
    xy.i.t[is.na(xy.i.t)] <- 0
    # Fl\u00e4che
    r.list <- r.variance.fun(xy.i.t[, 2:3], length(trakte[, 1]))
    ntns.f.ant[1, 1, i] <- r.list$R.xy * A
    ntns.f.ant[1, 2, i] <- sqrt(r.list$V.R.xy) * A
    # Fl\u00e4chenanteil
    xy.i.t <- merge(xy.t[TRUE, c("TNr", "n")], xy.ntns.i.t, by = c("TNr"), all.x = T)
    xy.i.t[is.na(xy.i.t)] <- 0
    r.list <- r.variance.fun(xy.i.t[, 2:3], length(trakte[, 1]))
    ntns.f.ant[2, 1, i] <- r.list$R.xy * 100
    ntns.f.ant[2, 2, i] <- sqrt(r.list$V.R.xy) * 100
  }
  # HBF des Stratums
  xy.i.t <- merge(trakte[TRUE, c("TNr", "m")], xy.t, by = "TNr", all.x = T)
  xy.i.t[is.na(xy.i.t)] <- 0
  r.list <- r.variance.fun(xy.i.t[, 2:3], length(trakte[, 1]))

  return(list(
    Datum = Sys.time(),
    HBF = r.list$R.xy * A, se.HBF = sqrt(r.list$V.R.xy) * A,
    NTNS = c(
      "sehr naturnah", "naturnah", "bedingt naturnah",
      "kulturbetont", "kulturbestimmt", "keine Angabe"
    ),
    NTNS.Flaeche.Anteil = ntns.f.ant
  ))
} # Ende <ntns.stratum.fun>
