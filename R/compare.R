#' Compare Some Current Version of Gerald's Functions With Mine
#'
#' We tend to produce code that won't auto-merge.
#' This is an attempt to minimize the effort when merging the two lines.
#' It requires my/the reference to be organized in code files giving one
#' function only and being named thereafter.
#' So might we need
#' \code{\link[fritools:split_code_file]{fritools::split_code_file}}
#' to split my code first.
#'
#' @param file A file from which to read input from.
#' @param styler Run \code{\link[styler:style_dir]{styler::style_dir}} on the 
#' files created.
#' @param out_dir An optional directory for output.
#' @param encoding The encoding to pass to `source` when reading the \code{file}.
#' @keywords internal
#' @export
#' @examples
#' \dontrun{
#'   splits <- split_functions_file(file = system.file("test_data",
#'                                                     "BWI3_HR_Funktionen_v3.r",
#'                                                     package = "bundeswaldinventur")
#'                                  )
#'   diff(result = splits)
#' }
split_functions_file <- function(file, styler = TRUE,
                                 out_dir = tempfile(), encoding = "latin1") {
    dir.create(out_dir, recursive = TRUE)
    e <- new.env()
    source(file, local = e, echo = FALSE, keep.source = TRUE,
           encoding = encoding)
    result <- NULL
    for (f in ls(e)) {
        if (is.function(e[[f]])) {
            content <- utils::capture.output(e[[f]])
            fun <- utils::head(content, -1)
            fun[1] <- paste(f, "<-", fun [1])
            out_path <- file.path(out_dir, paste0(f, ".R"))
            writeLines(fun, out_path)
            result <- c(result, out_path)
        }
    }
    if (isTRUE(styler)) styler::style_file(path = result)
    return(result)
}

diff <- function(result, reference = "~/bundeswaldinventur/R/") {
    if (fritools::is_installed("diffuse")) {
        for (result_path in result) {
            reference_path <- file.path(reference, basename(result_path))
            system(paste("diffuse", result_path, reference_path))
        }
    }
}


