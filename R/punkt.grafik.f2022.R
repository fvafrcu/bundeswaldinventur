#Punkte-Grafik
punkt.grafik.f2022 <- function(grupp.xy.tab,titel,x.lab,y.lab,grupp.col,axis.text.angle)
  #<grupp.xy.tab>: Tabelle (data.frame) mit den Werten:
  #<grupp>:  Reihe, z.B. BWI "2012", "2022" 
  #<x> die Bezeichnung der Abszisse (z.B. Altersklasse: "1-20", "21-40" usw.)
  #<y> Werte (= Balkenhöhe), z.B. Fläche
  #<kat.lab>, <x.lab>, <y.lab>: Bezeichnung der jeweiligen Attribute, z.B. "BWI",
  #"Altersklasse", "Fläche [ha]"
  #<grupp.col> Farben der Kategorien, z.B. c("grey","black")  
{
  p <- ggplot(data=grupp.xy.tab, aes(x=grupp.xy.tab[,2],y=grupp.xy.tab[,3], group=grupp.xy.tab[,1])) +
    scale_color_manual(values=grupp.col) + geom_point(aes(color=BA.Gruppe), linewidth=1.2)
  p + labs(title = titel, x = x.lab, y = y.lab)
}
