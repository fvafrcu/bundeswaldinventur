#' Erzeugt Durchmesserklassen-Beschriftung
#'
#' Erzeugt ein Label fuer die Durchmesserklassen-Beschriftung aus der
#' Klassifizierungsregel und der Anzahl der Durchmesserklassen.
#'
#' @author Gerald Kaendler \email{gerald.kaendler@@forst.bwl.de}
#' @section Erstellungsdatum: 16.07.2014
#' @section Korrekturdatum: 07.02.2015
#' @section Note: Anzahl der Durchmesserklassen wird vom rufenden Programm
#'  errechnet.
#' @param D.klass vorgegebene Klassifizierungsregel.
#' @param D.k Anzahl der Durchmesserklassen.
#' @export
#' @return Label fuer Durchmesserklassen-Beschriftung.
dkl.lab.fun <- function(D.klass, D.k) {
  dkl.lab <- rep(0, D.k)
  if (D.k > 1) {
    if (D.klass[[4]]) {
      dkl.lab[1] <- "0-6.9"
      dkl.lab[2] <- paste("7-", D.klass[[3]] - 0.1, sep = "")
      for (ii in 3:(D.k - 1)) {
        dkl.lab[ii] <- paste((ii - 2) * D.klass[[3]], "-", (ii - 1) * D.klass[[3]] - 0.1,
          sep = ""
        )
      }
    } else {
      for (ii in 1:(D.k - 1)) {
        dkl.lab[ii] <- paste(D.klass[[3]] * (ii - 1), "-", D.klass[[3]] * ii - 0.1, sep = "")
      }
      dkl.lab[D.k] <- paste(">", D.klass[[1]], sep = "")
    }
    dkl.lab[D.k] <- paste(">=", D.klass[[2]], sep = "")
  } else {
    dkl.lab[D.k] <- paste(D.klass[[1]], "-", D.klass[[2]], sep = "")
  } # k\u00e4/07.02.2015

  return(dkl.lab)
}
