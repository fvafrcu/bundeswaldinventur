#' Berechnet den relativen Anteil einer Untermenge
#'
#' Funktion berechnet den relativen Anteil des \code{substratums} im
#' \code{stratum}, welches das uebergeordnete Stratum (Obermenge) definiert.
#'
#' @author Gerald Kaendler \email{gerald.kaendler@@forst.bwl.de}
#' @section version: 18.11.2014
#' 10.07.2024, Cullmann: updated to use lower names, fixed hardcoded trakte.3,
#' now using nrow(x) instead of length(x[1,]).
#' @param stratum Liste, welche die Eckenmerkmale enthaelt nachdem das
#'  uebergeordnete Stratum (Obermenge) definiert wird.
#' @param substratum Liste, welche die Eckenmermale enthaelt nachdem das
#'  Substratum (Untermenge) definiert wird.
#' @param ecken Traktecken-Merkmale, die zur Stratenbildung dienen.
#' @param trakte Trakt-Kennwerte (m, m_HB, m_bHB, m_Wa).
#' @inheritParams fvbn.kreis.fun.1
#' @export
#' @return Liste mit relativen Anteil des Substratums am Stratum und dessen
#'  Standardfehler.
fl.proz.stratum.fun <- function(stratum, substratum, ecken, trakte) {
    names(stratum) <- tolower(names(stratum))
    names(substratum) <- tolower(names(substratum))
    names(ecken) <- tolower(names(ecken))
  te <- stratum.fun(stratum, ecken)
  if (nrow(te) == 0) {
    value <- list(Fl_Proz = NA, SE_Fl_Proz = NA)
  } else {
    nte.t <- stats::aggregate(rep(1, nrow(te)),
      by = list(te[["tnr"]]), sum
    )
    names(nte.t) <- c("tnr", "nte")
    substratum.1 <- stratum
    k <- length(names(substratum))
    for (i in 1:k) {
      substratum.1[[names(substratum)[i]]] <- substratum[[i]]
    }
    te.s <- stratum.fun(substratum.1, ecken)
    if (nrow(te.s) == 0) {
      value <- list(Fl_Proz = NA, SE_Fl_Proz = NA)
    } else {
      nte.s.t <- stats::aggregate(rep(1, nrow(te.s)),
        by = list(te.s[["tnr"]]), sum
      )
      names(nte.s.t) <- c("tnr", "nte.s")
      utils::head(nte.s.t)
      nte.s.t <- merge(nte.t, nte.s.t, by = "tnr", all.x = T)
      nte.s.t[is.na(nte.s.t)] <- 0
      r.list <- r.variance.fun(nte.s.t[, 2:3], nrow(trakte))
      value <- list(
        Fl_Proz = r.list[["R.xy"]] * 100,
        SE_Fl_Proz = r.list[["V.R.xy"]]^0.5 * 100
      )
    }
  }
  return(value)
}
