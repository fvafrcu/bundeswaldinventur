balken.grafik.f2022.2 <- function(kat.xy.tab,titel,x.lab,y.lab,kat.col)
#<kat.xy.tab>: Tabelle (data.frame) mit den Werten:
#<kat>: Kategorie oder Reihe, z.B. BWI "2012", "2022" 
#<x> die Bezeichnung der Abszisse (z.B. Altersklasse: "1-20", "21-40" usw.)
#<y> Werte (= Balkenhöhe), z.B. Fläche
#<kat.lab>, <x.lab>, <y.lab>: Bezeichnung der jeweiligen Attribute, z.B. "BWI",
#"Altersklasse", "Fläche [ha]"
#<kat.col> Farben der Kategorien, z.B. c("grey","black")
{
  p <- ggplot(kat.xy.tab, aes(x=kat.xy.tab[,2],y=kat.xy.tab[,3], fill=kat.xy.tab[,1])) +
  geom_bar(stat="identity",color="black",position = position_dodge()) +
  labs(title = titel, x = x.lab, y = y.lab, fill = names(kat.xy.tab)[1])
  
  p + scale_fill_manual(values=kat.col)
  
}
