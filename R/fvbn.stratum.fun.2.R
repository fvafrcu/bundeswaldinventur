#' Zustandshochrechnung fuer ein Stratum
#'
#' Funktion berechnet Standard-FVBN-Auswertung fuer die Befundeinheit
#' \code{auswahl} fuer die in der \code{eig.list} aufgefuehrten
#' Eigentumskategorien {gw, stw, kw, oew, pw, gpw, mpw, kpw} sowie die in
#' \code{bwi.list} aufgefuehrten BWI-Aufnahmen {1,2,3}. Es wird die Funktion
#' \code{\link{FVBN.bagrupp.akl.dkl.stratum.fun.2d}} verwendet, welche neben
#' Baumartengruppen auch die Summenwerte fuer alle Baumarten liefert.
#'
#' @author Gerald Kaendler \email{gerald.kaendler@@forst.bwl.de}
#' @section Erstellungsdatum: 02.12.2014
#' @param auswahl auswahl Liste, welche die Eckenmerkmale mit den Werten
#'  enthaelt, anhand derer die Auswahl fuer das Stratum erfolgt. Bsp.:
#'  list(Wa=c(3,5), Begehbar=1).
#' @param eig.list Liste mit Eigentumskategorien (moegliche Kategorien: gw, stw,
#'  kw, oew, pw, gpw, mpw, kpw).
#' @param bwi.list Liste mit BWI-Aufnahmen (1, 2, 3).
#' @param bagr Liste mit Baumarten-Zusammenfassungen zu Baumgruppen mit
#'  Bezeichner der Baumarten-Gruppen ("lab") z.B. list(bagr.lab = c("FiTa",
#'  "DglKiLae", "Bu", "Ei", "BLb", "WLb"), ba.grupp =list(c(10:19,30:39,90:99),
#'  c(20:29,40,50,51), c(100), c(110,111), c(112:199),c(200:299))).
#' @param a.klass Liste mit den Klassifizierungsparametern fuers Alter: z.B.
#'  list(A.ob=160, A.b=20).
#' @param d.klass Liste mit den Klassifizierungsparametern fuer Durchmesser z.B.
#'  list(D.unt=0, D.ob=70, D.b=10, Ndh=T), Ndh (Nicht-Derbholz) = T bedeutet,
#'  dass zusaetzlich Nicht-Dh (unter 7 cm) ausgewiesen wird, sonst gilt
#'  \code{D.unt} als unterste Schwelle.
#' @inheritParams fvbn.kreis.fun.1
#' @export
#' @return Dataframe-Tabelle mit FVBN-Auswertung nach Eigentumsklasse und
#'  aufgelisteter BWI.
fvbn.stratum.fun.2 <- function(auswahl, eig.list, bwi.list, bagr, a.klass, d.klass,
                               A = get_design("a", 3),
                               A.12 = get_design("a", 1),
                               trakte.3 = get_data("trakte.3"),
                               ecken.3 = get_data("ecken.3"),
                               baeume.3 = get_data("baeume.3"),
                               trakte.2 = get_data("trakte.2"),
                               ecken.2 = get_data("ecken.2"),
                               baeume.2 = get_data("baeume.2"),
                               trakte.1 = get_data("trakte.1"),
                               ecken.1 = get_data("ecken.1"),
                               baeume.1 = get_data("baeume.1")) {
  k <- length(eig.list)
  eig.list <- toupper(eig.list)
  eig.list <- sub("STW", "StW", eig.list)
  # k\u00e4/12.12.14
  l <- length(bwi.list)
  fvbn.bagr.stratum <- list()

  for (i in 1:k)
  {
    auswahl.i <- auswahl # k\u00e4/12.12.14
    if (eig.list[i] == "OEW") {
      auswahl.i$EigArt <- c("BW", "StW", "KW")
    } else
    if (eig.list[i] %in% c("GPW", "MPW", "KPW")) {
      auswahl.i$EigArt2 <- eig.list[i]
    } else
    if (eig.list[i] %in% c("BW", "StW", "KW", "PW")) {
      auswahl.i$EigArt <- eig.list[i]
    }
    # BWI
    for (j in 1:l)
    {
      if (bwi.list[j] == 1) {
        auswahl.i$Wa <- c(1:3)
        baeume <- baeume.1
        ecken <- ecken.1
        trakte <- trakte.1
        A.i <- A.12
        inv <- 1
      } else
      if (bwi.list[j] == 2) {
        auswahl.i$Wa <- c(1:3)
        baeume <- baeume.2
        ecken <- ecken.2
        trakte <- trakte.2
        A.i <- A.12
        inv <- 2
      } else {
        auswahl.i$Wa <- c(3, 5)
        baeume <- baeume.3
        ecken <- ecken.3
        trakte <- trakte.3
        A.i <- A
        inv <- 2
      }
      index <- (j - 1) * k + i

      fvbn.bagr.stratum[[index]] <- FVBN.bagrupp.akl.dkl.stratum.fun.2d(
        baeume, ecken, trakte, A.i, inv, bagr, a.klass, d.klass, auswahl.i
      )

      fvbn.bagr.stratum[[index]]$Eigentumsart <- eig.list[i]
      fvbn.bagr.stratum[[index]]$BWI <- bwi.list[j]
    }
  }

  return(fvbn.bagr.stratum)
} # Ende <fvbn.stratum.fun.2>
