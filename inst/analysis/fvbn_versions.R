pkgload::load_all()
FVBN <- FVBN.bagrupp.akl.dkl.stratum.fun.2e
re <- FVBN(
  get_data("baeume.3")[1:10, TRUE], get_data("ecken.3"),
  get_data("trakte.3"), get_design("a", 3), 2,
  get_bwi_species_groups("bc"),
  list(A.ob = 200, A.b = 60),
  list(
    D.unt = 0, D.ob = 50, D.b = 25,
    Ndh = T
  ),
  list(Wa = c(3, 5), Begehbar = 1)
)
FVBN <- FVBN.bagrupp.akl.dkl.stratum.fun.2d
rd <- FVBN(
  get_data("baeume.3")[1:10, TRUE], get_data("ecken.3"),
  get_data("trakte.3"), get_design("a", 3), 2,
  get_bwi_species_groups("bc"),
  list(A.ob = 200, A.b = 60),
  list(
    D.unt = 0, D.ob = 50, D.b = 25,
    Ndh = T
  ),
  list(Wa = c(3, 5), Begehbar = 1)
)
FVBN <- FVBN.bagrupp.akl.dkl.stratum.fun.2c
rc <- FVBN(
  get_data("baeume.3")[1:10, TRUE], get_data("ecken.3"),
  get_data("trakte.3"), get_design("a", 3), 2,
  get_bwi_species_groups("bc"),
  list(A.ob = 200, A.b = 60),
  list(
    D.unt = 0, D.ob = 50, D.b = 25,
    Ndh = T
  ),
  list(Wa = c(3, 5), Begehbar = 1)
)
FVBN <- FVBN.bagrupp.akl.dkl.stratum.fun.2b
rb <- FVBN(
  get_data("baeume.3")[1:10, TRUE], get_data("ecken.3"),
  get_data("trakte.3"), get_design("a", 3), 2,
  get_bwi_species_groups("bc"),
  list(A.ob = 200, A.b = 60),
  list(
    D.unt = 0, D.ob = 50, D.b = 25,
    Ndh = T
  ),
  list(Wa = c(3, 5), Begehbar = 1)
)
FVBN <- FVBN.bagrupp.akl.dkl.stratum.fun.2a
ra <- FVBN(
  get_data("baeume.3")[1:10, TRUE], get_data("ecken.3"),
  get_data("trakte.3"), get_design("a", 3), 2,
  get_bwi_species_groups("bc"),
  list(A.ob = 200, A.b = 60),
  list(
    D.unt = 0, D.ob = 50, D.b = 25,
    Ndh = T
  ),
  list(Wa = c(3, 5), Begehbar = 1)
)
identical(re$T.FVBN.Bagr.Akl.Dkl[, , 2, 2, 3], rd$T.FVBN.Bagr.Akl.Dkl[, , 2, 2, 3])
message("Added ", re$AKL[!re$AKL %in% rb$AKL], ", ", re$DKL[!re$DKL %in% rb$DKL])
identical(rd$T.FVBN.Bagr.Akl.Dkl[, , 2, 2, 3], rc$T.FVBN.Bagr.Akl.Dkl[, , 2, 2, 3])
message("Added ", rd$BAGR[!rd$BAGR %in% rc$BAGR])
identical(rc$T.FVBN.Bagr.Akl.Dkl[1:6, , 2, 2, 3], rb$T.FVBN.Bagr.Akl.Dkl[1:6, , 2, 2, 3])
message("Added ", rc$Attribute1[!re$Attribute1 %in% rb$Attribute1])
all.equal(rb$FVBN.ha.Bagr.Akl.Dkl[2:6, , 2, 2, 3], ra$FVBN.ha.Bagr.Akl.Dkl[, , 2, 2, 3])
message("Added ", rb$Attribute1[!rb$Attribute2 %in% ra$Attribute2])
