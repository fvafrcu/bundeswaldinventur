# bundeswaldinventur 0.31.0.9000

* FIXME

# bundeswaldinventur 0.31.0

* Add `get_game_damage()` as new version of verbiss.bagrupp.fun(), trying to
  solve different game damage codes.

# bundeswaldinventur 0.30.0

* Reorganize Dkl in `FVBN.bagrupp.akl.dkl.stratum.f2022.2f()`

# bundeswaldinventur 0.29.0

* Fix `N.art` in `VB.A.bagrupp.akl.dkl.stratum.f2022.3()`. Result array had
  wrong dimension for `N.art = FALSE`.
* Fix `verjg.kl4.bagrupp.fun()`. Had a column referenced by hardcoding a
  number...

# bundeswaldinventur 0.28.0

* Fixed `fl.stratum.fun()`, it used a hard coding to `trakte.3`, which gave
  errors different to those report in "Der Wald in Baden-W&uuml;rttemberg
  (2014)".
* Moved old deadwood functions to maintenance/deadwood.

# bundeswaldinventur 0.27.0

* Added argument `n` to `get_bwi_colors()`.

# bundeswaldinventur 0.26.0

* Added argument `is_compatibility` to `FVBN.bagrupp.akl.dkl.stratum.f2022.2f()`.
* Fix `dkl.lab.f2022()` to return labels that match those returned from 
  `dkl.lab.fun.R`.

# bundeswaldinventur 0.25.0

* Adapted `stratum.fun()` to work with data from package `bwidata`.
* Set default to `use_classic = TRUE` in call to `stratum.fun()`.
* Added `get_district_codes()`.

# bundeswaldinventur 0.24.0

* Added countrys area for 2022 as reported by
  https://www.statistik-bw.de/BevoelkGebiet/GebietFlaeche/GB-FlNutzung-AdV.js.
* Excluded tests for gitlab, as reporting by gitlab is poor.

# bundeswaldinventur 0.23.0

* Fixed tests.
* Added functions for BWI2022 from git/cs/fvafrcu/bwi2022hochrechnungen/,
  so this repo is obsolete.

# bundeswaldinventur 0.22.0

* Allow for only one merkmal in `habitat_baeume()`.
* Linked `biotop.biotop.baume.fun()` to `habitat_baeume()`
* Added `predict()` as an alternative to the wrappers 
  `FVBN.bagrupp.akl.dkl.stratum.fun.2[a-e]()`. Allows for logical subsets.
* Added argument `merkmale` to `biotop.baeume.fun` to let you select the
  attributes defining biotop trees.

# bundeswaldinventur 0.21.0

* `FVBN.bagrupp.akl.dkl.stratum.fun.2e` now `stop()`s when an inconsistent age
  class definition by `A.klass` would screw up the sums over all age classes.
  Added argment `is_ignore_inconsistent_aklass` to force old behauviour (of not
  throwing an error).
* New function `clean_log()` is a unit testing helper to clean logs from lists.
* Function `FVBN.bagrupp.akl.dkl.stratum.fun.2e` now allows for argument
  `A.klass` to be an arbitrary vector used for building the classes.
* Added arguemt `lowercase_names` to `get_data()`, allowing for lowercase
  conversion of `data.frame` names (reads option 
  `bundeswaldinventur[["lowercase_names"]]` by default.)
* Fixed local tests for R 4.0.0 using `stringsAsFactors = FALSE`.

# bundeswaldinventur 0.20.0

* Changed argument `name_arrays` of `FVBN.bagrupp.akl.dkl.stratum.fun.2e()` to 
  `is_name_arrays`!

# bundeswaldinventur 0.19.0

* styled with styler.

# bundeswaldinventur 0.18.0

* The statistics arrays for totals and per hectar values
  ("T.FVBN.Bagr.Akl.Dkl" and "FVBN.ha.Bagr.Akl.Dkl") in the list
  returned `by FVBN.bagrupp.akl.dkl.stratum.fun.2e()` so far had no
  dimnames, so we always had to look at the structure of the result where
  dimensions are defined and the had to figure it out.
  We added an argument `name_arrays` to `FVBN.bagrupp.akl.dkl.stratum.fun.2e()`` 
  to optionally write names to those arrays' dimensions.

# bundeswaldinventur 0.17.3

* Adapted test for R-devel 4.0.0 running check --as-cran.
* Improved test coverage.
* Added argument `jacobian` to `r.variance.fun()`.

# bundeswaldinventur 0.17.2

* Add exception handling for tests failing for factor levels ordered
  differently.

# bundeswaldinventur 0.17.1

* Passes gitlab-ci.

# bundeswaldinventur 0.17.0

* Fixed encoding issues for the data provided with the package.
* Used resave option to R CMD build to have smaller data files.

# bundeswaldinventur 0.16.0

* Internalized undeclared global objects from regional.R

# bundeswaldinventur 0.15.1

* Fixed failing test.

# bundeswaldinventur 0.15.0

* Finished test cases for all functions except the ones in R/batch.R and
  R/regional.R
* Changed return value of add\_colSums\_prettify\_and\_print\_xtable() from TRUE
  to a string of html code.
* Fixed bug in  get\_bwi\_species\_groups("bc").
* Passed package argument through get\_global\_objects() to get\_package\_data()
  to enable testing (without package bwibw).

# bundeswaldinventur 0.14.0

* Fixed argument tests functions for deadwood for BWI 2 (former tests forced BWI 3)
* Added handling of empty selections from stratification for stats::aggregate().

# bundeswaldinventur 0.13.1

* Improved on testing.
* Added workaround for evaluating tests/testthat.R via R CMD check and covr.
* Fixed Astmerkmale in stamm.merkmale.bagr.akl.t fun().

# bundeswaldinventur 0.13.0

* Fixed Astmerkmale in stamm.merkmale.bagr.fun().

# bundeswaldinventur 0.12.2

* Fix data aquistion for historical analysis scripts.
* Improved on testing with local data.

# bundeswaldinventur 0.12.1

* Fixed get\_data()

# bundeswaldinventur 0.12.0

* Added resampled data, disabled tests an on data from the bwibw data package.

# bundeswaldinventur 0.11.2

* Fix 0.11.1

# bundeswaldinventur 0.11.1

* Exclude testthat runs by R CMD check on external machines and disable data
  getter if the bwibw data package is not installed.

# bundeswaldinventur 0.11.0

* Added functions to load data and global variables into .GlobalEnv for
  historical analysis scripts.

# bundeswaldinventur 0.10.1

* Added man files to git.

# bundeswaldinventur 0.10.0

* Purged data files from git's history.

# bundeswaldinventur 0.9.0

* Get rid of data and global variables.
  Both can be found in hochrechnungen/functions/ and hochrechnungen/data.

# bundeswaldinventur 0.8.0

* Got rid of statistics data.
* Added imports from ggplot2.
* Exported functions from R/regional.R.
* Introduced argument `have_title` to plotting functions to get rid of 
  global object `TITLE\_PLOT`.

# bundeswaldinventur 0.7.3

* Fixed subsetting/indexing bug in FVBN...().

# bundeswaldinventur 0.7.2

* Fixed subset / indexing bug in verjg.kl4.bagrupp.fun(),
  see R/BWI\_HR.R lines 3502ff.

* Fixed fix 0.7.1 using 1L instead of 1 in testing length with
  identical().

# bundeswaldinventur 0.7.1

* Fixed subset / indexing bug in verjg.kl4.bagr.fun(),
  see R/BWI\_HR.R lines 3346ff.

# bundeswaldinventur 0.7.0

* Exported Objects defined R/global\_variables.R this makes the scripts in 
  hochrechnungen/regional/ run.

# bundeswaldinventur 0.6.0

* Renamed function get\_species\_groups() to group\_district\_species() in
  regional.R to avoid two functions of the same name (get\_species\_groups is 
  already assigned in variable_getters.R).

# bundeswaldinventur 0.5.0

* Replaced global\_variables.RData with assignments in global\_variables.R

# bundeswaldinventur 0.4.0

* Replaced stratum.fun with refactored version for speed.
* Added a `NEWS.md` file to track changes to the package.



